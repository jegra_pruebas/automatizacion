﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ID3070_PLD_Lista_del_SAT.Pruebas
{
    class Vinculo_PFAE
    {
        IWebDriver driver;

        [SetUp]
        public void Initializate()
        {
            driver = new FirefoxDriver();
        }

        [Test]
        public void LoginTest()
        {
            //Parametros iniciales
            var conquien = "ROSALIA ROSAS ROSAS";

            //Login
            driver.Navigate().GoToUrl("http://sistema.arrendamas.com/pruebas/arrendamas/index.php?salir=1");
            driver.FindElement(By.Id("login")).SendKeys("x.asoriano");
            driver.FindElement(By.Id("password")).SendKeys("apruebas");
            driver.FindElement(By.ClassName("botonBloque")).Click();
            System.Threading.Thread.Sleep(5000);

            //Entrar a Admin clientes
            IWebElement EnterClientes = driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[2]/div[2]/div/div[2]/div/a[3]"));
            Actions action1 = new Actions(driver); //referencia para posicionarnos en el botón de crear solicitud
            action1.MoveToElement(EnterClientes).Perform();
            IWebElement ClickSolicitud = driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[2]/div[2]/div/div[2]/div/a[3]/span[2]/span[1]"));
            ClickSolicitud.Click();
            System.Threading.Thread.Sleep(5000);

            //Variable búsqueda nombre
            var nombre = "ALVARO CASTIELLO GUTIERREZ";

            //Buscar al cliente
            driver.FindElement(By.CssSelector("input.campoTexto")).SendKeys(nombre);

            //Seleccionar campo búsqueda
            SelectElement nombre_busqueda = new SelectElement(driver.FindElement(By.Id("campo_busqueda")));
            driver.FindElement(By.Id("campo_busqueda")).Click();
            var busqueda_seleccionar = driver.FindElement(By.Id("campo_busqueda"));
            var selectElement_busqueda = new SelectElement(busqueda_seleccionar);
            selectElement_busqueda.SelectByValue("nombre");

            //Clic en buscar
            driver.FindElement(By.CssSelector(".botonBloque")).Click();


            System.Threading.Thread.Sleep(5000);

            //Entrar a la entrevista de una PFAE
            //Botón entrevista
            driver.FindElement(By.CssSelector(".opciones-fila > a:nth-child(6) > img:nth-child(1)")).Click();
            System.Threading.Thread.Sleep(5000);

            //Seleccionar el entidad de nacimiento
            SelectElement estado = new SelectElement(driver.FindElement(By.Id("id_estado")));
            driver.FindElement(By.Id("id_estado")).Click();
            var estado_seleccionar = driver.FindElement(By.Id("id_estado"));
            var selectElement = new SelectElement(estado_seleccionar);
            selectElement.SelectByText("Puebla");

            //Seleccionar ciudad de nacimiento
            SelectElement municipio = new SelectElement(driver.FindElement(By.Id("id_municipio")));
            driver.FindElement(By.Id("id_municipio")).Click();
            var municipio_seleccionar = driver.FindElement(By.Id("id_municipio"));
            var selectElement_municipio = new SelectElement(municipio_seleccionar);
            System.Threading.Thread.Sleep(3000);
            selectElement_municipio.SelectByValue("8");

            //Seleccionar actividad economica
            SelectElement actividad = new SelectElement(driver.FindElement(By.Id("id_empresa_actividad")));
            driver.FindElement(By.Id("id_empresa_actividad")).Click();
            var actividad_seleccionar = driver.FindElement(By.Id("id_empresa_actividad"));
            var selectElement_actividad = new SelectElement(actividad_seleccionar);
            System.Threading.Thread.Sleep(3000);
            selectElement_actividad.SelectByValue("232");

            //Seleccionar operaciones internacionales
            SelectElement operaciones_internacionales = new SelectElement(driver.FindElement(By.Id("operaciones_internacionales")));
            driver.FindElement(By.Id("operaciones_internacionales")).Click();
            var operaciones_seleccionar = driver.FindElement(By.Id("operaciones_internacionales"));
            var selectElement_operaciones = new SelectElement(operaciones_seleccionar);
            System.Threading.Thread.Sleep(3000);
            selectElement_operaciones.SelectByValue("EXPORTACIÓN");

            //Registrar curp
            driver.FindElement(By.Id("curp")).SendKeys("MASA890925MPLL09");

            //Fiel
            driver.FindElement(By.Id("fiel")).SendKeys("12345678");

            //Mantiene vínculo patrimonial
            SelectElement mantiene_vinculo = new SelectElement(driver.FindElement(By.Id("mantiene_vinculo_patrimonial")));
            driver.FindElement(By.Id("mantiene_vinculo_patrimonial")).Click();
            var vinculo_seleccionar = driver.FindElement(By.Id("mantiene_vinculo_patrimonial"));
            var selectElement_vinculo = new SelectElement(vinculo_seleccionar);
            System.Threading.Thread.Sleep(3000);
            selectElement_vinculo.SelectByValue("Si");

            driver.FindElement(By.Id("involucrado_vinculo_patrimonial")).SendKeys(conquien);

            driver.FindElement(By.Id("boton_guardar_datos")).Click();

        }



    }
}
