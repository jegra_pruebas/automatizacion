﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;

namespace ID3070_PLD_Lista_del_SAT.Pruebas
{
    class PropietarioRealPF
    {
        IWebDriver driver;

        [SetUp]
        public void Initializate()
        {
            driver = new FirefoxDriver();
        }

        [Test]
        public void Login()
        {
            driver.Navigate().GoToUrl("http://sistema.arrendamas.com/fake/arrendamas/index.php?salir=1");
            driver.FindElement(By.Id("login")).SendKeys("x.asoriano");
            driver.FindElement(By.Id("password")).SendKeys("aproduccion");
            driver.FindElement(By.ClassName("botonBloque")).Click();
            System.Threading.Thread.Sleep(5000);

            var url = "http://sistema.arrendamas.com/pruebas/arrendamas/solicitud/agregar_editar_propietario_real.php?ssid=26455316395&id_solicitud=6827&id_wf_estatus=76&id_workflow=1";

            driver.Navigate().GoToUrl(url);
            System.Threading.Thread.Sleep(5000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("window.scrollBy(0,2000)");

            driver.FindElement(By.CssSelector("a.ligaRegistro:nth-child(7)")).Click();

            // Agregar y editar (Nombre, apellidos y RFC) PARA CADA PERSONA DENTRO DE LA SOLICITUD
            // Agregar y eliminar (PM, PFAE, PF)


            
            driver.Navigate().GoToUrl("http://sistema.arrendamas.com/pruebas/arrendamas/solicitud/agregar_editar_propietario_real.php?ssid=1080940571&id_solicitud=6798&id_wf_estatus=81&id_workflow=1");
            
            driver.FindElement(By.CssSelector(".pestanasSup > li:nth-child(1) > a:nth-child(1)")).Click();
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.CssSelector(".pestanasSup > li:nth-child(1) > a:nth-child(1)")).Click();
            System.Threading.Thread.Sleep(5000);
            
           
            driver.FindElement(By.Id("pm_1")).Click();
            System.Threading.Thread.Sleep(2000);
            driver.FindElement(By.Id("pm_nombre_razon_social_1")).SendKeys("SERVICIOS AGRICOLAS");
            driver.FindElement(By.CssSelector("#agregarPropietario_1 > img:nth-child(1)")).Click();
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("pf_2")).Click();

            driver.FindElement(By.Id("nombre")).SendKeys("MINERVA");
            driver.FindElement(By.Id("apellido_paterno")).SendKeys("ALANIS");
            driver.FindElement(By.Id("apellido_materno")).SendKeys("BADILLO");
            driver.FindElement(By.Id("fecha_nacimiento")).SendKeys("2018-11-17");

            
            SelectElement estado = new SelectElement(driver.FindElement(By.Id("id_estado")));
            driver.FindElement(By.Id("id_estado")).Click();
            var opcionestado = driver.FindElement(By.Id("id_estado"));
            var seleccionar_estado = new SelectElement(opcionestado);
            System.Threading.Thread.Sleep(3000);
            seleccionar_estado.SelectByValue("1");
            System.Threading.Thread.Sleep(3000);

            SelectElement municipio = new SelectElement(driver.FindElement(By.Id("id_municipio")));
            driver.FindElement(By.Id("id_municipio")).Click();
            var opcionmunicipio = driver.FindElement(By.Id("id_municipio"));
            var seleccionar_municipio = new SelectElement(opcionmunicipio);
            System.Threading.Thread.Sleep(3000);
            seleccionar_municipio.SelectByValue("43");
            System.Threading.Thread.Sleep(3000);
            
            SelectElement actividad = new SelectElement(driver.FindElement(By.Id("id_empresa_actividad")));
            driver.FindElement(By.Id("id_empresa_actividad")).Click();
            var opcionactividad = driver.FindElement(By.Id("id_empresa_actividad"));
            var seleccionar_actividad = new SelectElement(opcionactividad);
            System.Threading.Thread.Sleep(3000);
            seleccionar_actividad.SelectByValue("6");
            System.Threading.Thread.Sleep(3000);
            IJavaScriptExecutor jrs = (IJavaScriptExecutor)driver;
            jrs.ExecuteScript("window.scrollBy(0,2000)");

            driver.FindElement(By.Id("calle_fiscal")).SendKeys("LAS PALMERAS");
            driver.FindElement(By.Id("numero_exterior_fiscal")).SendKeys("45");
            driver.FindElement(By.Id("codigo_postal_fiscal")).SendKeys("45138");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("codigo_postal_empresa")).SendKeys("45138");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("numero_interior_empresa")).Click();
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("boton_validar_coincidencias")).Click();
            driver.FindElement(By.Id("indicador_mensaje")).Click();
            driver.FindElement(By.Id("boton_submit_forma")).Click();
           
        }
    }
}
