﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;

namespace ID3070_PLD_Lista_del_SAT.Pruebas
{
    class Registro_Garante_PF
    {

        IWebDriver driver;

        [SetUp]
        public void Initializate()
        {
            driver = new FirefoxDriver();
        }

        [Test]
        public void LoginTest()
        {
            //Login
            driver.Navigate().GoToUrl("http://sistema.arrendamas.com/pruebas/arrendamas/index.php?salir=1");
            driver.FindElement(By.Id("login")).SendKeys("ines.soto");
            driver.FindElement(By.Id("password")).SendKeys("pruebas");
            driver.FindElement(By.ClassName("botonBloque")).Click();
            System.Threading.Thread.Sleep(5000);

            driver.Navigate().GoToUrl("http://sistema.arrendamas.com/pruebas/patron/modulos/ejecucion_workflow/editar_forma_estatus.php?id_workflow=1&id_wf_estatus=85&id_solicitud=6798&ver=1");
            System.Threading.Thread.Sleep(5000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("window.scrollBy(0,2000)");

            driver.FindElement(By.CssSelector("a.ligaRegistro:nth-child(7)")).Click();
            driver.FindElement(By.CssSelector(".botonBloque")).Click();
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("nombre")).SendKeys("RAMÓN");
            driver.FindElement(By.Id("apellido_paterno")).SendKeys("RUIZ");
            driver.FindElement(By.Id("apellido_materno")).SendKeys("RIVERA");
            driver.FindElement(By.Id("fecha_nacimiento")).SendKeys("2018-11-05");
            System.Threading.Thread.Sleep(3000);
            SelectElement pais = new SelectElement(driver.FindElement(By.Id("id_pais")));
            driver.FindElement(By.Id("id_pais")).Click();
            var opcionpais = driver.FindElement(By.Id("id_pais"));
            var seleccionar_pais = new SelectElement(opcionpais);
            System.Threading.Thread.Sleep(3000);
            seleccionar_pais.SelectByText("MÉXICO");
            System.Threading.Thread.Sleep(3000);
            SelectElement estado = new SelectElement(driver.FindElement(By.Id("id_estado")));
            driver.FindElement(By.Id("id_estado")).Click();
            var opcionestado = driver.FindElement(By.Id("id_estado"));
            var seleccionar_estado = new SelectElement(opcionestado);
            System.Threading.Thread.Sleep(2000);
            seleccionar_estado.SelectByText("Aguascalientes");
            System.Threading.Thread.Sleep(3000);
            SelectElement municipio = new SelectElement(driver.FindElement(By.Id("id_municipio")));
            driver.FindElement(By.Id("id_municipio")).Click();
            var opcionmunicipio = driver.FindElement(By.Id("id_municipio"));
            var seleccionar_municipio = new SelectElement(opcionmunicipio);
            System.Threading.Thread.Sleep(2000);
            seleccionar_municipio.SelectByValue("142");
            System.Threading.Thread.Sleep(3000);
            driver.FindElement(By.Id("nacionalidad")).Click();

            SelectElement actividad = new SelectElement(driver.FindElement(By.Id("id_empresa_actividad")));
            driver.FindElement(By.Id("id_empresa_actividad")).Click();
            var opcionactividad = driver.FindElement(By.Id("id_empresa_actividad"));
            var seleccionar_actividad = new SelectElement(opcionactividad);
            System.Threading.Thread.Sleep(2000);
            driver.FindElement(By.Id("forma_pago")).Click();
            seleccionar_actividad.SelectByValue("6");
            System.Threading.Thread.Sleep(3000);
            IJavaScriptExecutor jss = (IJavaScriptExecutor)driver;
            jss.ExecuteScript("window.scrollBy(0,2000)");
            driver.FindElement(By.Id("email")).SendKeys("ISOTO.JEGRA@GMAIL.COM");
            driver.FindElement(By.Id("calle_fiscal")).SendKeys("LAS PALMERAS");
            driver.FindElement(By.Id("numero_exterior_fiscal")).SendKeys("45");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("codigo_postal_fiscal")).SendKeys("45138");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("codigo_postal_empresa")).SendKeys("45138");
            driver.FindElement(By.Id("numero_exterior_empresa")).Click();
            System.Threading.Thread.Sleep(5000);
            
            driver.FindElement(By.Id("validarGarante")).Click();
           


        }

        
        }
    }
