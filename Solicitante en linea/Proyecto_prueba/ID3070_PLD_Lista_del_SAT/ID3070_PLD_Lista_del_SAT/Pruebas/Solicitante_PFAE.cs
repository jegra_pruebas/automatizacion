﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ID3070_PLD_Lista_del_SAT.Pruebas
{
    class Solicitante_PFAE
    {
        IWebDriver driver;

        [SetUp]
        public void Initializate()
        {
            driver = new FirefoxDriver();
        }

        [Test]
        public void LoginTest()
        {
            //PARAMETROS PARA PRUEBAS
            var nombre = "ALBERTO JOSÉ";           
            var apellido_p = "GONZALEZ";
            var apellido_m = "Cortez";
            var rfc = "AeOJ790126665";
            var fecha = "1979-01-26";

            //Login
            driver.Navigate().GoToUrl("http://74.127.61.116/pruebas/arrendamas/solicitante2007/menu_solicitante.php");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.CssSelector(".personaFisicaEmpresarial")).Click();
            driver.FindElement(By.CssSelector("#div_progress > div:nth-child(1) > div:nth-child(1) > div:nth-child(5) > input:nth-child(1)")).Click();
            driver.FindElement(By.CssSelector("#wizard_pfae_botones > a:nth-child(2) > img:nth-child(1)")).Click();

            //Bloque datos generales
            driver.FindElement(By.Id("forma_nombre")).SendKeys(nombre);
            driver.FindElement(By.Id("forma_apellido_paterno")).SendKeys(apellido_p);
            driver.FindElement(By.Id("forma_apellido_materno")).SendKeys(apellido_m);
            driver.FindElement(By.Id("forma_rfc")).SendKeys(rfc);
            driver.FindElement(By.Id("forma_email")).SendKeys("amartinez.jegra@gmail.com");
            driver.FindElement(By.Id("forma_celular_codigo_ciudad")).SendKeys("67");
            driver.FindElement(By.Id("forma_celular_numero")).SendKeys("89676759");

            //Seleccionar el género del cliente
            SelectElement genero = new SelectElement(driver.FindElement(By.Id("forma_sexo")));
            driver.FindElement(By.Id("forma_sexo")).Click();
            var sexo = driver.FindElement(By.Id("forma_sexo"));
            var selectElement = new SelectElement(sexo);
            //System.Threading.Thread.Sleep(1000);
            selectElement.SelectByText("Masculino");

            //Seleccionar la fecha de nacimiento
            driver.FindElement(By.Id("forma_fecha_nacimiento")).SendKeys(fecha);
            //Nacionalidad
            driver.FindElement(By.Id("forma_nacionalidad")).SendKeys("Mexicana");
            //Seleccionar Estado civil del cliente
            SelectElement estadoci = new SelectElement(driver.FindElement(By.Id("forma_estado_civil")));
            driver.FindElement(By.Id("forma_estado_civil")).Click();
            var estadoc = driver.FindElement(By.Id("forma_estado_civil"));
            var selectElement1 = new SelectElement(estadoc);
            //( System.Threading.Thread.Sleep(1000);
            selectElement1.SelectByValue("Soltero");

            //Seleccionar identificación
            SelectElement identificacion = new SelectElement(driver.FindElement(By.Id("forma_identificacion")));
            driver.FindElement(By.Id("forma_identificacion")).Click();
            var identi = driver.FindElement(By.Id("forma_identificacion"));
            var selectElement2 = new SelectElement(identi);
            //( System.Threading.Thread.Sleep(1000);
            selectElement2.SelectByValue("INE");

            //Seleccionar Grado máximo de estudios
            SelectElement GradoM = new SelectElement(driver.FindElement(By.Id("forma_grado_maximo_estudios")));
            driver.FindElement(By.Id("forma_grado_maximo_estudios")).Click();
            var grado = driver.FindElement(By.Id("forma_grado_maximo_estudios"));
            var selectGrado = new SelectElement(grado);
            //( System.Threading.Thread.Sleep(1000);
            selectGrado.SelectByValue("Ninguno");

            driver.FindElement(By.Id("forma_dependientes_economicos_numero")).SendKeys("0");
            driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[1]/div[1]/div[3]/div/div/div[4]/a[2]/img")).Click();


            //BLOQUE INFORMACIÓN FISCAL
            //domicilio fiscal
            driver.FindElement(By.Id("forma_facturacion_calle")).SendKeys("San rafael");
            driver.FindElement(By.Id("forma_facturacion_numero_exterior")).SendKeys("2002");
            driver.FindElement(By.Id("forma_facturacion_numero_interior")).SendKeys("4");
            //Seleccionar país
            var opcionPais_IF = driver.FindElement(By.Id("forma_facturacion_id_pais"));
            var seleccionarPais2 = new SelectElement(opcionPais_IF);
            System.Threading.Thread.Sleep(4000);//Tiempo de espera
            seleccionarPais2.SelectByValue("435");
            //Código postal
            driver.FindElement(By.Id("forma_facturacion_codigo_postal")).SendKeys("45138");
            System.Threading.Thread.Sleep(4000);
            //Seleccionar colonia
            var opcionColonia2 = driver.FindElement(By.CssSelector("#forma_combo_facturacion_colonia"));
            var seleccionarColonia2 = new SelectElement(opcionColonia2);
            System.Threading.Thread.Sleep(4000);
            seleccionarColonia2.SelectByText("EL TIGRE");
            //Tomar datos de domicilio fiscal
            driver.FindElement(By.Id("copy_domicilio_fiscal")).Click();
            System.Threading.Thread.Sleep(4000);
            driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[1]/div[1]/div[3]/div/div/div[4]/a[2]/img")).Click();

            //BLOQUE VIVIENDA
            SelectElement vivienda = new SelectElement(driver.FindElement(By.Id("forma_vivienda_tipo")));
            driver.FindElement(By.Id("forma_vivienda_tipo")).Click();
            var opcionvivienda = driver.FindElement(By.Id("forma_vivienda_tipo"));
            var seleccionarVivienda = new SelectElement(opcionvivienda);
            //( System.Threading.Thread.Sleep(1000);
            seleccionarVivienda.SelectByValue("Propia");
            driver.FindElement(By.Id("forma_vivienda_valor_aproximado")).SendKeys("10000");
            driver.FindElement(By.Id("forma_vivienda_anios_residencia")).SendKeys("2");
            driver.FindElement(By.Id("forma_vivienda_meses_residencia")).SendKeys("24");
            driver.FindElement(By.Id("forma_vivienda_codigo_ciudad")).SendKeys("67");
            driver.FindElement(By.Id("forma_vivienda_numero")).SendKeys("89676745");
            driver.FindElement(By.Id("forma_vivienda_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_vivienda_numero_exterior")).SendKeys("56");
            //Seleccionar país
            var opcionPais = driver.FindElement(By.Id("forma_vivienda_id_pais"));
            var seleccionarPais = new SelectElement(opcionPais);
            System.Threading.Thread.Sleep(4000);//Tiempo de espera
            seleccionarPais.SelectByValue("435");
            //Código postal
            driver.FindElement(By.Id("forma_vivienda_codigo_postal")).SendKeys("45138");
            //Seleccionar colonia
            var opcionColonia = driver.FindElement(By.Id("forma_combo_vivienda_colonia"));
            var seleccionarColonia = new SelectElement(opcionColonia);
            System.Threading.Thread.Sleep(4000);
            seleccionarColonia.SelectByText("EL TIGRE");
            driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[1]/div[1]/div[3]/div/div/div[4]/a[2]/img")).Click();

            //BLOQUE EMPLEO
            driver.FindElement(By.Id("forma_profesion_actual")).SendKeys("Ingeniero");
            driver.FindElement(By.Id("forma_empresa_puesto_actual")).SendKeys("Gerente");
            var opcionPosicion = driver.FindElement(By.Id("forma_empresa_posicion"));
            var selectPosicion = new SelectElement(opcionPosicion);
            System.Threading.Thread.Sleep(5000);
            selectPosicion.SelectByText("Propietario");
            driver.FindElement(By.Id("forma_empresa_fecha_ingreso")).SendKeys("2011-01-21");
            driver.FindElement(By.Id("forma_empresa_nombre")).SendKeys("EMPRESA");
            var opcioncontrato = driver.FindElement(By.Id("forma_empresa_tipo_contrato"));
            var seleccionarcontrato = new SelectElement(opcioncontrato);
            System.Threading.Thread.Sleep(4000);
            seleccionarcontrato.SelectByText("Fijo");
            driver.FindElement(By.Id("forma_empresa_codigo_ciudad")).SendKeys("33");
            driver.FindElement(By.Id("forma_empresa_numero")).SendKeys("56341289");
            driver.FindElement(By.Id("forma_empresa_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_empresa_numero_exterior")).SendKeys("78");
            var opcionPaisEmpresa = driver.FindElement(By.Id("forma_empresa_id_pais"));
            var seleccionarpaisempresa = new SelectElement(opcionPaisEmpresa);
            System.Threading.Thread.Sleep(5000);
            seleccionarpaisempresa.SelectByValue("435");
            driver.FindElement(By.Id("forma_empresa_codigo_postal")).SendKeys("45138");
            System.Threading.Thread.Sleep(5000);
            var opcionColoniaEmpresa = driver.FindElement(By.Id("forma_combo_empresa_colonia"));
            var seleccionarcolonia = new SelectElement(opcionColoniaEmpresa);
            System.Threading.Thread.Sleep(5000);
            seleccionarcolonia.SelectByText("COLEGIO DEL AIRE");
            driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[1]/div[1]/div[3]/div/div/div[4]/a[2]/img")).Click();

            //BLOQUE INGRESOS Y EGRESOS
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_ingresos_mensuales_fijos")).SendKeys("50000");
            driver.FindElement(By.Id("forma_egresos_gasto_familiar")).SendKeys("21000");
            driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[1]/div[1]/div[3]/div/div/div[4]/a[2]/img")).Click();
            System.Threading.Thread.Sleep(5000);

            //BLOQUE ACTIVOS Y PASIVOS
            driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[1]/div[1]/div[3]/div/div/div[4]/a[2]/img")).Click();
            System.Threading.Thread.Sleep(5000);

            //BLOQUE INFORMACIÓN BANCARIA
            /*
            var opcionInstitucion = driver.FindElement(By.Id("forma_cuentas_captacion_id_banco"));
            var seleccionarbanco = new SelectElement(opcionInstitucion);
            seleccionarbanco.SelectByValue("8");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_cuentas_captacion_tipo_inversion")).SendKeys("CHEQUES");
            driver.FindElement(By.Id("forma_cuentas_captacion_numero_contrato_tarjeta")).SendKeys("56341234567889098765");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[1]/div[1]/div[3]/div/div/div[4]/a[2]/img")).Click();
            System.Threading.Thread.Sleep(5000);
            */

            //BLOQUE REFERENCIAS PERSONALES
            driver.FindElement(By.Id("forma_familiar_nombre_completo")).SendKeys("RAFAELA DURAN");
            var opcionParentesco = driver.FindElement(By.Id("forma_familiar_parentesco_tiempo_conocerlo"));
            var seleccionarParentesco = new SelectElement(opcionParentesco);
            seleccionarParentesco.SelectByText("Padre");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_familiar_calle")).SendKeys("LAS PALMERAS");
            driver.FindElement(By.Id("forma_familiar_numero_exterior")).SendKeys("78");
            driver.FindElement(By.Id("forma_familiar_colonia")).SendKeys("LA TUZANIA");
            System.Threading.Thread.Sleep(5000);
            var estadoreferencia = driver.FindElement(By.Id("familiar_id_municipio_estado"));
            var seleccionarEstadoReferencia = new SelectElement(estadoreferencia);
            seleccionarEstadoReferencia.SelectByText("Aguascalientes");
            System.Threading.Thread.Sleep(5000);
            var ciudadreferencia = driver.FindElement(By.Id("familiar_id_municipio_id_municipio"));
            var seleccionarciudadreferencia = new SelectElement(ciudadreferencia);
            seleccionarciudadreferencia.SelectByText("Asientos");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_familiar_telefono_codigo_ciudad")).SendKeys("22");
            driver.FindElement(By.Id("forma_familiar_telefono_numero")).SendKeys("23322323");


            driver.FindElement(By.Id("forma_personal_nombre_completo")).SendKeys("RICARDO");
            driver.FindElement(By.Id("forma_personal_parentesco_tiempo_conocerlo")).SendKeys("4");
            driver.FindElement(By.Id("forma_personal_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_personal_numero_exterior")).SendKeys("89");
            driver.FindElement(By.Id("forma_personal_colonia")).SendKeys("La Tuzania");
            System.Threading.Thread.Sleep(5000);
            var estadoreferenciapersonal = driver.FindElement(By.Id("personal_id_municipio_estado"));
            var seleccionarEstadoReferenciapersonal = new SelectElement(estadoreferenciapersonal);
            seleccionarEstadoReferenciapersonal.SelectByText("Aguascalientes");
            System.Threading.Thread.Sleep(5000);
            var ciudadreferenciapersonal = driver.FindElement(By.Id("personal_id_municipio_id_municipio"));
            var seleccionarciudadreferenciapersonal = new SelectElement(ciudadreferenciapersonal);
            seleccionarciudadreferenciapersonal.SelectByText("Asientos");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_personal_telefono_codigo_ciudad")).SendKeys("45");
            driver.FindElement(By.Id("forma_personal_telefono_numero")).SendKeys("45342323");


            driver.FindElement(By.Id("forma_personal_secundario_nombre_completo")).SendKeys("Rita");
            driver.FindElement(By.Id("forma_personal_secundario_parentesco_tiempo_conocerlo")).SendKeys("2");
            driver.FindElement(By.Id("forma_personal_secundario_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_personal_secundario_numero_exterior")).SendKeys("2");
            driver.FindElement(By.Id("forma_personal_secundario_colonia")).SendKeys("La tuzania");
            System.Threading.Thread.Sleep(5000);
            var estadoreferenciapersonalsegundo = driver.FindElement(By.Id("personal_secundario_id_municipio_estado"));
            var seleccionarEstadoReferenciapersonalsegundo = new SelectElement(estadoreferenciapersonalsegundo);
            seleccionarEstadoReferenciapersonalsegundo.SelectByText("Aguascalientes");
            System.Threading.Thread.Sleep(5000);
            var ciudadreferenciapersonalsegundo = driver.FindElement(By.Id("personal_secundario_id_municipio_id_municipio"));
            var seleccionarciudadreferenciapersonalsegundo = new SelectElement(ciudadreferenciapersonalsegundo);
            seleccionarciudadreferenciapersonalsegundo.SelectByText("Asientos");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_personal_secundario_telefono_codigo_ciudad")).SendKeys("56");
            driver.FindElement(By.Id("forma_personal_secundario_telefono_numero")).SendKeys("25656565");
            driver.FindElement(By.CssSelector("#wizard_pfae_botones > a:nth-child(2) > img:nth-child(1)")).Click();
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[1]/div[1]/div[3]/div/div/div[4]/a[2]/img")).Click();
            System.Threading.Thread.Sleep(5000);


        }
    }
}
