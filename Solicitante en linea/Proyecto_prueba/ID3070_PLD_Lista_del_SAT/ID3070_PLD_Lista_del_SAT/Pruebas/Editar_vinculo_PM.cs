﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ID3070_PLD_Lista_del_SAT.Pruebas
{
    class Editar_vinculo_PM
    {
        IWebDriver driver;

        [SetUp]
        public void Initializate()
        {
            driver = new FirefoxDriver();
        }

        [Test]
        public void LoginTest()
        {
            //Parametros iniciales
            //Variable búsqueda nombre
            var nombre = "ESFERA DIVISION PUBLICIDAD, S.A. DE C.V.";
            //Variable vínculo patrimonial
            var conquien = "ROSALIA ROSAS ROSAS";

            //Login
            driver.Navigate().GoToUrl("http://sistema.arrendamas.com/pruebas/arrendamas/index.php?salir=1");
            driver.FindElement(By.Id("login")).SendKeys("x.asoriano");
            driver.FindElement(By.Id("password")).SendKeys("apruebas");
            driver.FindElement(By.ClassName("botonBloque")).Click();
            System.Threading.Thread.Sleep(5000);

            //Entrar a Admin clientes
            IWebElement EnterClientes = driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[2]/div[2]/div/div[2]/div/a[3]"));
            Actions action1 = new Actions(driver); //referencia para posicionarnos en el botón de crear solicitud
            action1.MoveToElement(EnterClientes).Perform();
            IWebElement ClickSolicitud = driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[2]/div[2]/div/div[2]/div/a[3]/span[2]/span[1]"));
            ClickSolicitud.Click();
            System.Threading.Thread.Sleep(5000);



            //Buscar al cliente
            driver.FindElement(By.CssSelector("input.campoTexto")).SendKeys(nombre);

            //Seleccionar campo búsqueda
            SelectElement nombre_busqueda = new SelectElement(driver.FindElement(By.Id("campo_busqueda")));
            driver.FindElement(By.Id("campo_busqueda")).Click();
            var busqueda_seleccionar = driver.FindElement(By.Id("campo_busqueda"));
            var selectElement_busqueda = new SelectElement(busqueda_seleccionar);
            selectElement_busqueda.SelectByValue("nombre");

            //Clic en buscar
            driver.FindElement(By.CssSelector(".botonBloque")).Click();

            System.Threading.Thread.Sleep(5000);

            //Entrar en a la entrevista PM
            driver.FindElement(By.CssSelector(".opciones-fila > a:nth-child(5) > img:nth-child(1)")).Click();

            //Mantiene vínculo patrimonial
            SelectElement mantiene_vinculo = new SelectElement(driver.FindElement(By.Id("mantiene_vinculo_patrimonial")));
            driver.FindElement(By.Id("mantiene_vinculo_patrimonial")).Click();
            var vinculo_seleccionar = driver.FindElement(By.Id("mantiene_vinculo_patrimonial"));
            var selectElement_vinculo = new SelectElement(vinculo_seleccionar);
            System.Threading.Thread.Sleep(3000);
            selectElement_vinculo.SelectByValue("Si");

            driver.FindElement(By.Id("involucrado_vinculo_patrimonial")).SendKeys(conquien);

            driver.FindElement(By.Id("boton_guardar_datos")).Click();

        }
    }
}