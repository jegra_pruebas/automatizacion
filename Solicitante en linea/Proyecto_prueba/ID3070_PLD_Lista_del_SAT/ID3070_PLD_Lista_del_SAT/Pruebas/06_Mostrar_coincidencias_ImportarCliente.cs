﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ID3070_PLD_Lista_del_SAT.Pruebas
{
    class _06_Mostrar_coincidencias_ImportarCliente
    {
        IWebDriver driver;

        [SetUp]
        public void Initializate()
        {
            driver = new FirefoxDriver();
        }

        [Test]
        public void LoginTest()
        {
            //Login
            driver.Navigate().GoToUrl("http://sistema.arrendamas.com/pruebas/arrendamas/index.php?salir=1");
            driver.FindElement(By.Id("login")).SendKeys("x.asoriano");
            driver.FindElement(By.Id("password")).SendKeys("apruebas");
            driver.FindElement(By.ClassName("botonBloque")).Click();
            System.Threading.Thread.Sleep(5000);

            //Entrar a Admin clientes
            IWebElement EnterClientes = driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[2]/div[2]/div/div[2]/div/a[3]"));
            Actions action1 = new Actions(driver); //referencia para posicionarnos en el botón de crear solicitud
            action1.MoveToElement(EnterClientes).Perform();
            IWebElement ClickSolicitud = driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[2]/div[2]/div/div[2]/div/a[3]/span[2]/span[1]"));
            ClickSolicitud.Click();

            System.Threading.Thread.Sleep(5000);

            //String parentWindows = driver.CurrentWindowHandle;
            driver.FindElement(By.CssSelector(".pestanasSup > li:nth-child(3)")).Click(); //Ingresar a la pestaña importar
            System.Threading.Thread.Sleep(5000);

            foreach(String window in driver.WindowHandles)
            {
                driver.SwitchTo().Window(window);
            }

            Assert.False(driver.Title.Contains("Importar"));
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.CssSelector("div.cen:nth-child(2) > form:nth-child(1) > input:nth-child(5)")).Click();
            
           




        }

        /*
        [TearDown]
        public void EndTest()
        {
            driver.Quit();
        }*/

    }
    }
