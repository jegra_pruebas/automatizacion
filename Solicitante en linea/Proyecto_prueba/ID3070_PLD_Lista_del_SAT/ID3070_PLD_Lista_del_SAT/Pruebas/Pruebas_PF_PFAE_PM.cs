﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ID3070_PLD_Lista_del_SAT.Pruebas
{
    class Pruebas_PF_PFAE_PM
    {
        IWebDriver driver;

        [SetUp]
        public void Initializate()
        {
            driver = new FirefoxDriver();
        }

        protected void _login()
        {

            driver.Navigate().GoToUrl("http://sistema.arrendamas.com/pre_produccion/arrendamas/");
            driver.FindElement(By.Id("login")).SendKeys("x.rmontano");
            driver.FindElement(By.Id("password")).SendKeys("rmontanoii");
            driver.FindElement(By.ClassName("botonBloque")).Click();
            System.Threading.Thread.Sleep(5000);

            //Entrar a Admin clientes
            IWebElement EnterClientes = driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[2]/div[2]/div/div[2]/div/a[3]"));
            Actions action1 = new Actions(driver); //referencia para posicionarnos en el botón de crear solicitud
            action1.MoveToElement(EnterClientes).Perform();
            IWebElement ClickSolicitud = driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[2]/div[2]/div/div[2]/div/a[3]/span[2]/span[1]"));
            ClickSolicitud.Click();

            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[2]/ul[1]/li[2]/a")).Click();

        }

        protected void NextClick(bool es_solicitante, string tipo_persona)
        {

            driver.FindElement(By.CssSelector("#next_paso_solicitante > img:nth-child(1)")).Click();

        }

        [Test]
        public void PersonaFisicaTest()
        {

            //PARAMETROS PARA PRUEBAS
            var nombre = "Abril";
            var apellido_m = "Cortez";
            var apellido_p = "LOPEZ";
            var rfc = "AAAL410627T97";
            var fecha_n = "1941-06-27";
            var es_solicitante = false;

            //Login
            if(Environment.GetEnvironmentVariable("tipo") == "solicitante")
            {
                //driver.Navigate().GoToUrl("http://solicitudonline.arrendamas.com/pruebas/arrendamas/solicitante2007/menu_solicitante.php");
                driver.Navigate().GoToUrl("http://74.127.61.116/pruebas/arrendamas/solicitante2007/menu_solicitante.php");
                System.Threading.Thread.Sleep(5000);

                driver.FindElement(By.CssSelector(".personaFisica")).Click();
                driver.FindElement(By.CssSelector("#div_progress > div:nth-child(1) > div:nth-child(1) > div:nth-child(5) > input:nth-child(1)")).Click();
                driver.FindElement(By.CssSelector("#wizard_pf_botones > a:nth-child(2) > img:nth-child(1)")).Click();

                es_solicitante = true;
            }
            else
            {
                this._login();

                driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div/div[2]/div/div[2]/div[1]/a[1]")).Click();
                driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div/div[2]/ul/li[2]/a")).Click();

            }


            //Datos generales (Paso1)
            driver.FindElement(By.Id("forma_nombre")).SendKeys(nombre);
            driver.FindElement(By.Id("forma_apellido_paterno")).SendKeys(apellido_m);
            driver.FindElement(By.Id("forma_apellido_materno")).SendKeys(apellido_p);
            driver.FindElement(By.Id("forma_rfc")).SendKeys(rfc);
            driver.FindElement(By.Id("forma_email")).SendKeys("amartinez.jegra@gmail.com");
            driver.FindElement(By.Id("forma_celular_codigo_ciudad")).SendKeys("67");
            driver.FindElement(By.Id("forma_celular_numero")).SendKeys("89676759");

            //Seleccionar el género del cliente
            SelectElement genero = new SelectElement(driver.FindElement(By.Id("forma_sexo")));
            driver.FindElement(By.Id("forma_sexo")).Click();
            var sexo = driver.FindElement(By.Id("forma_sexo"));
            var selectElement = new SelectElement(sexo);
            //System.Threading.Thread.Sleep(1000);
            selectElement.SelectByText("Masculino");


            //Seleccionar País de nacimiento
            SelectElement pais = new SelectElement(driver.FindElement(By.Id("forma_id_pais")));
            driver.FindElement(By.Id("forma_id_pais")).Click();
            var pais_nacimiento = driver.FindElement(By.Id("forma_id_pais"));
            var select_pais = new SelectElement(pais_nacimiento);
            //( System.Threading.Thread.Sleep(1000);
            select_pais.SelectByValue("435");

            //Seleccionar nacionalidad
            driver.FindElement(By.Id("forma_nacionalidad")).SendKeys("Mexicana");


            //Seleccionar la fecha de nacimiento
            driver.FindElement(By.Id("forma_fecha_nacimiento")).SendKeys(fecha_n);
            //Seleccionar Estado civil del cliente
            SelectElement estadoci = new SelectElement(driver.FindElement(By.Id("forma_estado_civil")));
            driver.FindElement(By.Id("forma_estado_civil")).Click();
            var estadoc = driver.FindElement(By.Id("forma_estado_civil"));
            var selectElement1 = new SelectElement(estadoc);
            //( System.Threading.Thread.Sleep(1000);
            selectElement1.SelectByValue("Soltero");

            //Seleccionar identificación
            SelectElement identificacion = new SelectElement(driver.FindElement(By.Id("forma_identificacion")));
            driver.FindElement(By.Id("forma_identificacion")).Click();
            var identi = driver.FindElement(By.Id("forma_identificacion"));
            var selectElement2 = new SelectElement(identi);
            //( System.Threading.Thread.Sleep(1000);
            selectElement2.SelectByValue("INE");

            //Seleccionar Grado máximo de estudios
            SelectElement GradoM = new SelectElement(driver.FindElement(By.Id("forma_grado_maximo_estudios")));
            driver.FindElement(By.Id("forma_grado_maximo_estudios")).Click();
            var grado = driver.FindElement(By.Id("forma_grado_maximo_estudios"));
            var selectGrado = new SelectElement(grado);
            //( System.Threading.Thread.Sleep(1000);
            selectGrado.SelectByValue("Ninguno");

            driver.FindElement(By.Id("forma_dependientes_economicos_numero")).SendKeys("0");

            this.NextClick(es_solicitante, "pf");

            //
            /////

            //BLOQUE VIVIENDA
            SelectElement vivienda = new SelectElement(driver.FindElement(By.Id("forma_vivienda_tipo")));
            driver.FindElement(By.Id("forma_vivienda_tipo")).Click();
            var opcionvivienda = driver.FindElement(By.Id("forma_vivienda_tipo"));
            var seleccionarVivienda = new SelectElement(opcionvivienda);
            //( System.Threading.Thread.Sleep(1000);
            seleccionarVivienda.SelectByValue("Propia");
            driver.FindElement(By.Id("forma_vivienda_valor_aproximado")).SendKeys("10000");
            driver.FindElement(By.Id("forma_vivienda_anios_residencia")).SendKeys("2");
            driver.FindElement(By.Id("forma_vivienda_meses_residencia")).SendKeys("24");
            driver.FindElement(By.Id("forma_vivienda_codigo_ciudad")).SendKeys("67");
            driver.FindElement(By.Id("forma_vivienda_numero")).SendKeys("89676745");
            driver.FindElement(By.Id("forma_vivienda_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_vivienda_numero_exterior")).SendKeys("56");
            //Seleccionar país
            var opcionPais = driver.FindElement(By.Id("forma_vivienda_id_pais"));
            var seleccionarPais = new SelectElement(opcionPais);
            System.Threading.Thread.Sleep(4000);//Tiempo de espera
            seleccionarPais.SelectByValue("435");
            //Código postal
            driver.FindElement(By.Id("forma_vivienda_codigo_postal")).SendKeys("45138");
            //Seleccionar colonia
            var opcionColonia = driver.FindElement(By.Id("forma_combo_vivienda_colonia"));
            var seleccionarColonia = new SelectElement(opcionColonia);
            System.Threading.Thread.Sleep(4000);
            seleccionarColonia.SelectByValue("EL TIGRE");

            this.NextClick(es_solicitante, "pf");

            //BLOQUE EMPLEO
            driver.FindElement(By.Id("forma_profesion_actual")).SendKeys("Ingeniero");
            driver.FindElement(By.Id("forma_empresa_puesto_actual")).SendKeys("Gerente");
            var opcionPosicion = driver.FindElement(By.Id("forma_empresa_posicion"));
            var selectPosicion = new SelectElement(opcionPosicion);
           
            selectPosicion.SelectByText("Propietario");
            driver.FindElement(By.Id("forma_empresa_fecha_ingreso")).SendKeys("2011-01-21");
            driver.FindElement(By.Id("forma_empresa_nombre")).SendKeys("EMPRESA");
            var opcioncontrato = driver.FindElement(By.Id("forma_empresa_tipo_contrato"));
            var seleccionarcontrato = new SelectElement(opcioncontrato);
            System.Threading.Thread.Sleep(4000);
            seleccionarcontrato.SelectByText("Fijo");
            driver.FindElement(By.Id("forma_empresa_codigo_ciudad")).SendKeys("33");
            driver.FindElement(By.Id("forma_empresa_numero")).SendKeys("56341289");
            driver.FindElement(By.Id("forma_empresa_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_empresa_numero_exterior")).SendKeys("78");

            var opcionPaisEmpresa = driver.FindElement(By.Id("forma_empresa_id_pais"));
            var seleccionarpaisempresa = new SelectElement(opcionPaisEmpresa);
            System.Threading.Thread.Sleep(5000);
            seleccionarpaisempresa.SelectByValue("435");

            driver.FindElement(By.Id("forma_empresa_codigo_postal")).SendKeys("45138");
            System.Threading.Thread.Sleep(5000);
            var opcionColoniaEmpresa = driver.FindElement(By.Id("forma_combo_empresa_colonia"));
            var seleccionarcolonia = new SelectElement(opcionColoniaEmpresa);
            System.Threading.Thread.Sleep(5000);
            seleccionarcolonia.SelectByText("COLEGIO DEL AIRE");
            System.Threading.Thread.Sleep(5000);

            this.NextClick(es_solicitante, "pf");

            //BLOQUE INGRESOS Y EGRESOS
            System.Threading.Thread.Sleep(2000);
            driver.FindElement(By.Id("forma_ingresos_mensuales_fijos")).SendKeys("50000");
            driver.FindElement(By.Id("forma_egresos_gasto_familiar")).SendKeys("21000");

            this.NextClick(es_solicitante, "pf");

            System.Threading.Thread.Sleep(2000);

            //BLOQUE ACTIVOS Y PASIVOS
            this.NextClick(es_solicitante, "pf");

            System.Threading.Thread.Sleep(2000);

            //BLOQUE INFORMACIÓN BANCARIA
            /* SE OCULTA BLOQUE POR TEMA DE ID */

            /* Bloque eliminado 
            var opcionInstitucion = driver.FindElement(By.Id("forma_cuentas_captacion_id_banco"));
            var seleccionarbanco = new SelectElement(opcionInstitucion);
            seleccionarbanco.SelectByValue("8");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.CssSelector("#wizard_pf_botones > a:nth-child(2) > img"));
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_cuentas_captacion_tipo_inversion")).SendKeys("CHEQUES");
            driver.FindElement(By.Id("forma_cuentas_captacion_numero_contrato_tarjeta")).SendKeys("56341234567889098765");
            System.Threading.Thread.Sleep(5000);
            this.NextClick(es_solicitante, "pf");
            System.Threading.Thread.Sleep(5000);*/ 


            //BLOQUE REFERENCIAS PERSONALES
            driver.FindElement(By.Id("forma_familiar_nombre_completo")).SendKeys("RAFAELA DURAN");
            var opcionParentesco = driver.FindElement(By.Id("forma_familiar_parentesco_tiempo_conocerlo"));
            var seleccionarParentesco = new SelectElement(opcionParentesco);
            seleccionarParentesco.SelectByText("Padre");
            System.Threading.Thread.Sleep(2000);
            driver.FindElement(By.Id("forma_familiar_calle")).SendKeys("LAS PALMERAS");
            driver.FindElement(By.Id("forma_familiar_numero_exterior")).SendKeys("78");
            driver.FindElement(By.Id("forma_familiar_colonia")).SendKeys("LA TUZANIA");

            var estadoreferencia = driver.FindElement(By.Id("familiar_id_municipio_estado"));
            var seleccionarEstadoReferencia = new SelectElement(estadoreferencia);
            seleccionarEstadoReferencia.SelectByText("Aguascalientes");
            System.Threading.Thread.Sleep(3000);
            var ciudadreferencia = driver.FindElement(By.Id("familiar_id_municipio_id_municipio"));
            var seleccionarciudadreferencia = new SelectElement(ciudadreferencia);
            seleccionarciudadreferencia.SelectByText("Asientos");
            System.Threading.Thread.Sleep(3000);
            driver.FindElement(By.Id("forma_familiar_telefono_codigo_ciudad")).SendKeys("22");
            driver.FindElement(By.Id("forma_familiar_telefono_numero")).SendKeys("23322323");


            driver.FindElement(By.Id("forma_personal_nombre_completo")).SendKeys("RICARDO");
            driver.FindElement(By.Id("forma_personal_parentesco_tiempo_conocerlo")).SendKeys("4");
            driver.FindElement(By.Id("forma_personal_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_personal_numero_exterior")).SendKeys("89");
            driver.FindElement(By.Id("forma_personal_colonia")).SendKeys("La Tuzania");

            var estadoreferenciapersonal = driver.FindElement(By.Id("personal_id_municipio_estado"));
            var seleccionarEstadoReferenciapersonal = new SelectElement(estadoreferenciapersonal);
            seleccionarEstadoReferenciapersonal.SelectByText("Aguascalientes");
            System.Threading.Thread.Sleep(5000);
            var ciudadreferenciapersonal = driver.FindElement(By.Id("personal_id_municipio_id_municipio"));
            var seleccionarciudadreferenciapersonal = new SelectElement(ciudadreferenciapersonal);
            seleccionarciudadreferenciapersonal.SelectByText("Asientos");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_personal_telefono_codigo_ciudad")).SendKeys("45");
            driver.FindElement(By.Id("forma_personal_telefono_numero")).SendKeys("45342323");


            driver.FindElement(By.Id("forma_personal_secundario_nombre_completo")).SendKeys("Rita");
            driver.FindElement(By.Id("forma_personal_secundario_parentesco_tiempo_conocerlo")).SendKeys("2");
            driver.FindElement(By.Id("forma_personal_secundario_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_personal_secundario_numero_exterior")).SendKeys("2");
            driver.FindElement(By.Id("forma_personal_secundario_colonia")).SendKeys("La tuzania");
           
            var estadoreferenciapersonalsegundo = driver.FindElement(By.Id("personal_secundario_id_municipio_estado"));
            var seleccionarEstadoReferenciapersonalsegundo = new SelectElement(estadoreferenciapersonalsegundo);
            seleccionarEstadoReferenciapersonalsegundo.SelectByText("Aguascalientes");
            System.Threading.Thread.Sleep(5000);
            var ciudadreferenciapersonalsegundo = driver.FindElement(By.Id("personal_secundario_id_municipio_id_municipio"));
            var seleccionarciudadreferenciapersonalsegundo = new SelectElement(ciudadreferenciapersonalsegundo);
            seleccionarciudadreferenciapersonalsegundo.SelectByText("Asientos");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_personal_secundario_telefono_codigo_ciudad")).SendKeys("56");
            driver.FindElement(By.Id("forma_personal_secundario_telefono_numero")).SendKeys("25656565");
            driver.FindElement(By.CssSelector("#wizard_pf_botones > a:nth-child(2) > img:nth-child(1)")).Click();
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("finish_solicitante")).Click();
            System.Threading.Thread.Sleep(5000);


        }

        [Test]
        public void PersonaMoralTest()
        {
            var es_solicitante = false;

            //Variables para configurar datos del Titular
            var razon_social = "AQUAERIS ACUACULTURA Y ARQUITECTURA SUSTENTABLE, S.C.";
            var RFC = "AAA070908HL7";
            var fecha = "2009-09-09";

            //Variables para configurar datos del representante legal
            var nombre_representante_legal = "Inés";
            var paterno_representante_legal = "Soto";
            var materno_representante_legal = "López";
            //Por cada ejecución hay que modificar el RFC y/o la fecha de constitución 
            var rfc_representante_legal = "AABJ650718RI4";

            //Variables para configurar AVAL
            var nombre_aval = "Andra";
            var apellido_p_aval = "Rosas";
            var apellido_m_aval = "Rosas";
            var RFC_aval = "AAGD9110291PA";
            //Por cada ejecución hay que modificar el RFC y/o la fecha de constitución 
            var fecha_aval = "1991-10-29";

            //Login

            //Login
            if (Environment.GetEnvironmentVariable("tipo") == "solicitante")
            {
                driver.Navigate().GoToUrl("http://74.127.61.116/pruebas/arrendamas/solicitante2007/menu_solicitante.php");

                System.Threading.Thread.Sleep(5000);

                driver.FindElement(By.CssSelector(".personaMoral")).Click();

                System.Threading.Thread.Sleep(5000);

                driver.FindElement(By.CssSelector("#div_progress > div:nth-child(1) > div:nth-child(1) > div:nth-child(7) > input:nth-child(1)")).Click();
                driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();

                es_solicitante = true;
            }
            else
            {
                this._login();

                driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div/div[2]/div/div[2]/div[1]/a[3]")).Click();//PM
                driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div/div[2]/ul/li[2]/a")).Click();
            }

            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("forma_nombre")).SendKeys(razon_social);
            driver.FindElement(By.Id("forma_rfc")).SendKeys(RFC);

            //Seleccionar el sector del cliente
            SelectElement sector = new SelectElement(driver.FindElement(By.Id("forma_sector")));
            driver.FindElement(By.Id("forma_sector")).Click();
            var indu = driver.FindElement(By.Id("forma_sector"));
            var selectElement1 = new SelectElement(indu);
            System.Threading.Thread.Sleep(1000);
            selectElement1.SelectByText("Industria");


            //Seleccionar Actividad de la empresa

            if (es_solicitante)
            {
                SelectElement actividad = new SelectElement(driver.FindElement(By.Id("forma_id_empresa_actividad")));
                driver.FindElement(By.Id("contenedor_forma_id_empresa_actividad")).Click();
                var acabado = driver.FindElement(By.Id("forma_id_empresa_actividad"));
                var selectElement_actividad = new SelectElement(acabado);
                System.Threading.Thread.Sleep(1000);
                selectElement_actividad.SelectByText("ACABADO DE HILOS");
            }
            else
            {
                driver.FindElement(By.Id("contenedor_forma_id_empresa_actividad")).Click();
                System.Threading.Thread.Sleep(1000);
                driver.FindElement(By.XPath("/html/body/span/span/span[1]/input")).SendKeys("ACABADO DE HILOS");
                driver.FindElement(By.XPath("/html/body/span/span/span[1]/input")).SendKeys(OpenQA.Selenium.Keys.Enter);

            }

            //Número de empleados
            driver.FindElement(By.Id("forma_numero_empleados")).SendKeys("2");

            driver.FindElement(By.Id("forma_telefono_codigo_ciudad")).SendKeys("238");
            driver.FindElement(By.Id("forma_telefono_numero")).SendKeys("8967675");

            this.NextClick(es_solicitante,"pm");

            //---

            //BLOQUE INFORMACIÓN FISCAL
            //domicilio fiscal
            driver.FindElement(By.Id("forma_facturacion_calle")).SendKeys("San rafael");
            driver.FindElement(By.Id("forma_facturacion_numero_exterior")).SendKeys("2002");
            driver.FindElement(By.Id("forma_facturacion_numero_interior")).SendKeys("4");
            //Seleccionar país
            var opcionPais_IF = driver.FindElement(By.Id("forma_facturacion_id_pais"));
            var seleccionarPais2 = new SelectElement(opcionPais_IF);
            System.Threading.Thread.Sleep(4000);//Tiempo de espera
            seleccionarPais2.SelectByValue("435");
            //Código postal
            driver.FindElement(By.Id("forma_facturacion_codigo_postal")).SendKeys("45138");
            System.Threading.Thread.Sleep(4000);
            //Seleccionar colonia
            var opcionColonia2 = driver.FindElement(By.CssSelector("#forma_combo_facturacion_colonia"));
            var seleccionarColonia2 = new SelectElement(opcionColonia2);
            System.Threading.Thread.Sleep(4000);
            seleccionarColonia2.SelectByText("EL TIGRE");
            //Tomar datos de domicilio fiscal
            driver.FindElement(By.Id("copy_domicilio_fiscal")).Click();
            System.Threading.Thread.Sleep(4000);

            this.NextClick(es_solicitante, "pm");

            //BLOQUE INFORMACIÓN DE ESCRITURA
            driver.FindElement(By.Id("forma_fecha_constitucion")).SendKeys(fecha);

            this.NextClick(es_solicitante, "pm");

            //BLOQUE CONTACTOS
            //Representante legal
            driver.FindElement(By.Id("forma_representante_legal_nombre")).SendKeys(nombre_representante_legal);
            driver.FindElement(By.Id("forma_representante_legal_apellido_paterno")).SendKeys(paterno_representante_legal);
            driver.FindElement(By.Id("forma_representante_legal_apellido_materno")).SendKeys(materno_representante_legal);
            driver.FindElement(By.Id("forma_representante_legal_email")).SendKeys("isoto.jegra@gmail.com");
            driver.FindElement(By.Id("forma_representante_legal_rfc")).SendKeys(rfc_representante_legal);
            driver.FindElement(By.Id("forma_representante_legal_puesto")).SendKeys("GERENTE");
            driver.FindElement(By.Id("forma_representante_legal_codigo_ciudad")).SendKeys("22");
            driver.FindElement(By.Id("forma_representante_legal_telefono")).SendKeys("23322323");

            //Contacto para gestionar este contrato
            driver.FindElement(By.Id("check_copy_from_contrato")).Click();
            System.Threading.Thread.Sleep(4000);

            //Contacto de pago
            driver.FindElement(By.Id("check_copy_pago")).Click();
            System.Threading.Thread.Sleep(4000);

            //Administrador general
            driver.FindElement(By.Id("check_copy_administrador")).Click();
            System.Threading.Thread.Sleep(4000);

            this.NextClick(es_solicitante, "pm");

            //BLOQUE PODERES
            driver.FindElement(By.Id("forma_0_nombre")).SendKeys("Rosa");
            driver.FindElement(By.Id("forma_0_apellido_paterno")).SendKeys("Rosales");

            driver.FindElement(By.Id("forma_0_poder_1")).Click();
            driver.FindElement(By.Id("forma_0_poder_2")).Click();
            driver.FindElement(By.Id("forma_0_poder_3")).Click();

            this.NextClick(es_solicitante, "pm");

            //BLOQUE ACCIONISTA MAYORITARIO
            //Seleccionar el tipo de persona
            SelectElement tipo_persona = new SelectElement(driver.FindElement(By.Id("forma_0_tipo_persona")));
            driver.FindElement(By.Id("forma_0_tipo_persona")).Click();
            var opcionPM = driver.FindElement(By.Id("forma_0_tipo_persona"));
            var seleccionar_tipo_persona = new SelectElement(opcionPM);
            //( System.Threading.Thread.Sleep(1000);
            seleccionar_tipo_persona.SelectByValue("Moral");
            //compañia
            driver.FindElement(By.Id("forma_0_nombre")).SendKeys("JEGRA");
            driver.FindElement(By.Id("forma_0_rfc")).SendKeys("MASA890925mp");
            //Porcentaje
            driver.FindElement(By.Id("forma_0_porcentaje_participacion")).SendKeys("100");

            driver.FindElement(By.Id("forma_0_accionista_calle")).SendKeys("San rafael");
            driver.FindElement(By.Id("forma_0_accionista_numero_exterior")).SendKeys("12");
            driver.FindElement(By.Id("forma_0_accionista_numero_interior")).SendKeys("10");
            //Código postal y colonia
            driver.FindElement(By.Id("forma_0_accionista_codigo_postal")).SendKeys("45138");
            System.Threading.Thread.Sleep(5000);
            SelectElement colonia = new SelectElement(driver.FindElement(By.Id("forma_0_combo_accionista_colonia")));
            driver.FindElement(By.Id("forma_0_combo_accionista_colonia")).Click();
            var opcionAccionistaColonia = driver.FindElement(By.Id("forma_0_combo_accionista_colonia"));
            var seleccionar_colonia = new SelectElement(opcionAccionistaColonia);
            seleccionar_colonia.SelectByValue("EL TIGRE");

            System.Threading.Thread.Sleep(5000);

            this.NextClick(es_solicitante, "pm");

            //BLOQUE CUENTAS DE CAPTACIÓN
            /*
            System.Threading.Thread.Sleep(5000);
            SelectElement banco = new SelectElement(driver.FindElement(By.Id("forma_0_cuentas_captacion_id_banco")));
            driver.FindElement(By.Id("forma_0_cuentas_captacion_id_banco")).Click();
            var opcionBanco = driver.FindElement(By.Id("forma_0_cuentas_captacion_id_banco"));
            var seleccionar_banco = new SelectElement(opcionBanco);
            seleccionar_banco.SelectByValue("14");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("forma_0_cuentas_captacion_tipo_inversion")).SendKeys("CHEQUES");
            driver.FindElement(By.Id("forma_0_cuentas_captacion_clabe")).SendKeys("123443434343434343");
            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();
            */

            System.Threading.Thread.Sleep(5000);

            //BLOQUE REFERENCIAS(PROVEEDORES)
            driver.FindElement(By.Id("forma_0_nombre_completo")).SendKeys("LA ECONOMICA");
            driver.FindElement(By.Id("forma_0_nombre_contacto")).SendKeys("Luis Rivera");
            driver.FindElement(By.Id("forma_0_contacto_puesto")).SendKeys("Director");
            driver.FindElement(By.Id("forma_0_telefono_codigo_ciudad")).SendKeys("33");
            driver.FindElement(By.Id("forma_0_telefono_numero")).SendKeys("56341213");

            driver.FindElement(By.Id("forma_1_nombre_completo")).SendKeys("LA SURTIDA");
            driver.FindElement(By.Id("forma_1_nombre_contacto")).SendKeys("Pedro Rivera");
            driver.FindElement(By.Id("forma_1_contacto_puesto")).SendKeys("Gerente");
            driver.FindElement(By.Id("forma_1_telefono_codigo_ciudad")).SendKeys("33");
            driver.FindElement(By.Id("forma_1_telefono_numero")).SendKeys("56341213");
            this.NextClick(es_solicitante, "pm");
            System.Threading.Thread.Sleep(5000);


            //BLOQUE INFORMACIÓN FINANCIERA
            //Seleccionar período de ventas
            /*
            SelectElement periodoVentas = new SelectElement(driver.FindElement(By.Id("forma_ventas_periodo")));
            driver.FindElement(By.Id("forma_ventas_periodo")).Click();
            var opcionperiodo = driver.FindElement(By.Id("forma_ventas_periodo"));
            var seleccionar_periodo = new SelectElement(opcionperiodo);
            seleccionar_periodo.SelectByValue("Todo el año");
            System.Threading.Thread.Sleep(5000);
            //Seleccionar sistema de ventas
            SelectElement sistemaVentas = new SelectElement(driver.FindElement(By.Id("forma_ventas_sistema")));
            driver.FindElement(By.Id("forma_ventas_sistema")).Click();
            var opcionsistema = driver.FindElement(By.Id("forma_ventas_sistema"));
            var seleccionar_sistema = new SelectElement(opcionsistema);
            seleccionar_sistema.SelectByValue("Mayoreo");

            driver.FindElement(By.Id("forma_ventas_porcentaje_fabricacion")).SendKeys("12");
            driver.FindElement(By.Id("forma_ventas_porcentaje_compras")).SendKeys("12");

            driver.FindElement(By.Id("forma_estados_financieros_fecha_inicio")).SendKeys("2018-11-02");
            driver.FindElement(By.Id("forma_estados_financieros_fecha_fin")).SendKeys("2018-11-02");
            driver.FindElement(By.Id("forma_monto_capital_social")).SendKeys("34353");
            driver.FindElement(By.Id("forma_monto_utilidades_acumuladas")).SendKeys("213123");
            driver.FindElement(By.Id("forma_monto_ventas_totales")).SendKeys("234234");
            driver.FindElement(By.Id("forma_monto_activo_circulante")).SendKeys("45645");
            driver.FindElement(By.Id("forma_monto_activo_total")).SendKeys("534352");
            driver.FindElement(By.Id("forma_monto_pasivo_circulante")).SendKeys("526263");
            driver.FindElement(By.Id("forma_monto_pasivo_total")).SendKeys("536362");
            driver.FindElement(By.Id("forma_monto_capital_contable")).SendKeys("32422");

            driver.FindElement(By.Id("forma_anterior_estados_financieros_fecha_inicio")).SendKeys("2018-11-02");
            driver.FindElement(By.Id("forma_anterior_estados_financieros_fecha_fin")).SendKeys("2018-11-02");
            driver.FindElement(By.Id("forma_anterior_monto_capital_social")).SendKeys("34353");
            driver.FindElement(By.Id("forma_anterior_monto_utilidades_acumuladas")).SendKeys("213123");
            driver.FindElement(By.Id("forma_anterior_monto_ventas_totales")).SendKeys("234234");
            driver.FindElement(By.Id("forma_anterior_monto_activo_circulante")).SendKeys("45645");
            driver.FindElement(By.Id("forma_anterior_monto_activo_total")).SendKeys("534352");
            driver.FindElement(By.Id("forma_anterior_monto_pasivo_circulante")).SendKeys("526263");
            driver.FindElement(By.Id("forma_anterior_monto_pasivo_total")).SendKeys("536362");
            driver.FindElement(By.Id("forma_anterior_monto_capital_contable")).SendKeys("32422");
            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();
            System.Threading.Thread.Sleep(5000);
            */

            //BLOQUE AVAL
            driver.FindElement(By.Id("forma_nombre")).SendKeys(nombre_aval);
            driver.FindElement(By.Id("forma_apellido_paterno")).SendKeys(apellido_p_aval);
            driver.FindElement(By.Id("forma_apellido_materno")).SendKeys(apellido_m_aval);
            driver.FindElement(By.Id("forma_rfc")).SendKeys(RFC_aval);
            driver.FindElement(By.Id("forma_email")).SendKeys("isoto.jegra@gmail.com");
            driver.FindElement(By.Id("forma_celular_codigo_ciudad")).SendKeys("33");
            driver.FindElement(By.Id("forma_celular_numero")).SendKeys("34343434");

            SelectElement generoaval = new SelectElement(driver.FindElement(By.Id("forma_sexo")));
            driver.FindElement(By.Id("forma_sexo")).Click();
            var opciongenero = driver.FindElement(By.Id("forma_sexo"));
            var seleccionar_genero = new SelectElement(opciongenero);
            seleccionar_genero.SelectByValue("Masculino");

            driver.FindElement(By.Id("forma_fecha_nacimiento")).SendKeys(fecha_aval);

            //País de nacimiento
            SelectElement paisnacimiento = new SelectElement(driver.FindElement(By.Id("forma_id_pais")));
            driver.FindElement(By.Id("forma_id_pais")).Click();
            var opcionespais = driver.FindElement(By.Id("forma_id_pais"));
            var seleccionar_pais = new SelectElement(opcionespais);
            seleccionar_pais.SelectByValue("435");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("forma_nacionalidad")).SendKeys("Mexicana");

            SelectElement estadocivilaval = new SelectElement(driver.FindElement(By.Id("forma_estado_civil")));
            driver.FindElement(By.Id("forma_estado_civil")).Click();
            var opcionestadocivil = driver.FindElement(By.Id("forma_estado_civil"));
            var seleccionar_estadocivil = new SelectElement(opcionestadocivil);
            seleccionar_estadocivil.SelectByValue("Soltero");
            System.Threading.Thread.Sleep(1000);
            SelectElement identificacionaval = new SelectElement(driver.FindElement(By.Id("forma_identificacion")));
            driver.FindElement(By.Id("forma_identificacion")).Click();
            var opcionesidentificacion = driver.FindElement(By.Id("forma_identificacion"));
            var seleccionar_identificacion = new SelectElement(opcionesidentificacion);
            seleccionar_identificacion.SelectByValue("INE");
            System.Threading.Thread.Sleep(1000);
            SelectElement gradoestudios = new SelectElement(driver.FindElement(By.Id("forma_grado_maximo_estudios")));
            driver.FindElement(By.Id("forma_grado_maximo_estudios")).Click();
            var opcionesgrado = driver.FindElement(By.Id("forma_grado_maximo_estudios"));
            var seleccionar_grado = new SelectElement(opcionesgrado);
            seleccionar_grado.SelectByValue("Secundaria");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("forma_dependientes_economicos_numero")).SendKeys("0");
            this.NextClick(es_solicitante, "pm");


            //BLOQUE VIVIENDA
            SelectElement vivienda = new SelectElement(driver.FindElement(By.Id("forma_vivienda_tipo")));
            driver.FindElement(By.Id("forma_vivienda_tipo")).Click();
            var opcionvivienda = driver.FindElement(By.Id("forma_vivienda_tipo"));
            var seleccionarVivienda = new SelectElement(opcionvivienda);
            //( System.Threading.Thread.Sleep(1000);
            seleccionarVivienda.SelectByValue("Propia");
            driver.FindElement(By.Id("forma_vivienda_valor_aproximado")).SendKeys("10000");
            driver.FindElement(By.Id("forma_vivienda_anios_residencia")).SendKeys("2");
            driver.FindElement(By.Id("forma_vivienda_meses_residencia")).SendKeys("24");
            driver.FindElement(By.Id("forma_vivienda_codigo_ciudad")).SendKeys("67");
            driver.FindElement(By.Id("forma_vivienda_numero")).SendKeys("89676745");
            driver.FindElement(By.Id("forma_vivienda_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_vivienda_numero_exterior")).SendKeys("56");
            //Seleccionar país
            var opcionPais = driver.FindElement(By.Id("forma_vivienda_id_pais"));
            var seleccionarPais = new SelectElement(opcionPais);
            System.Threading.Thread.Sleep(4000);//Tiempo de espera
            seleccionarPais.SelectByValue("435");
            //Código postal
            driver.FindElement(By.Id("forma_vivienda_codigo_postal")).SendKeys("45138");

            //Seleccionar colonia
            SelectElement colonia1 = new SelectElement(driver.FindElement(By.Id("forma_combo_vivienda_colonia")));
            driver.FindElement(By.Id("forma_combo_vivienda_colonia")).Click();
            var opcionColonia = driver.FindElement(By.Id("forma_combo_vivienda_colonia"));
            var seleccionarColonia = new SelectElement(opcionColonia);

            seleccionarColonia.SelectByValue("EL TIGRE");
            System.Threading.Thread.Sleep(4000);


            this.NextClick(es_solicitante, "pm");

            //BLOQUE EMPLEO
            driver.FindElement(By.Id("forma_profesion_actual")).SendKeys("Ingeniero");
            driver.FindElement(By.Id("forma_empresa_puesto_actual")).SendKeys("Gerente");
            driver.FindElement(By.Id("forma_empresa_fecha_ingreso")).SendKeys("2018-11-05");
            driver.FindElement(By.Id("forma_empresa_nombre")).SendKeys("Servicios de limpieza");

            SelectElement tipocontrato = new SelectElement(driver.FindElement(By.Id("forma_empresa_tipo_contrato")));
            driver.FindElement(By.Id("forma_empresa_tipo_contrato")).Click();
            var opcionescontrato = driver.FindElement(By.Id("forma_empresa_tipo_contrato"));
            var seleccionar_contrato = new SelectElement(opcionescontrato);
            seleccionar_contrato.SelectByValue("Fijo");
           

            SelectElement tipoempresa = new SelectElement(driver.FindElement(By.Id("forma_empresa_tipo")));
            driver.FindElement(By.Id("forma_empresa_tipo")).Click();
            var opcionesempresa = driver.FindElement(By.Id("forma_empresa_tipo"));
            var seleccionar_empresa = new SelectElement(opcionesempresa);
            seleccionar_empresa.SelectByValue("Pública");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("forma_empresa_codigo_ciudad")).SendKeys("33");
            driver.FindElement(By.Id("forma_empresa_numero")).SendKeys("56341289");
            driver.FindElement(By.Id("forma_empresa_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_empresa_numero_exterior")).SendKeys("78");

            SelectElement pais_aval = new SelectElement(driver.FindElement(By.Id("forma_empresa_id_pais")));
            driver.FindElement(By.Id("forma_empresa_id_pais")).Click();
            var opcionespais1 = driver.FindElement(By.Id("forma_empresa_id_pais"));
            var seleccionar_pais1 = new SelectElement(opcionespais1);
            seleccionar_pais1.SelectByValue("435");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("forma_empresa_codigo_postal")).SendKeys("45138");
            System.Threading.Thread.Sleep(5000);
            SelectElement colonia_aval = new SelectElement(driver.FindElement(By.Id("forma_combo_empresa_colonia")));
            var opcionColoniaEmpresa = driver.FindElement(By.Id("forma_combo_empresa_colonia"));
            var seleccionarcolonia = new SelectElement(opcionColoniaEmpresa);
            System.Threading.Thread.Sleep(5000);
            seleccionarcolonia.SelectByText("COLEGIO DEL AIRE");
            this.NextClick(es_solicitante, "pm");



            //BLOQUE INGRESOS Y EGRESOS
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_ingresos_mensuales_fijos")).SendKeys("50000");
            driver.FindElement(By.Id("forma_egresos_gasto_familiar")).SendKeys("21000");
            this.NextClick(es_solicitante, "pm");
            System.Threading.Thread.Sleep(5000);

            //BLOQUE ACTIVOS Y PASIVOS
            this.NextClick(es_solicitante, "pm");
            System.Threading.Thread.Sleep(5000);

            //BLOQUE INFORMACIÓN BANCARIA
            /*
            System.Threading.Thread.Sleep(5000);
            SelectElement institucion = new SelectElement(driver.FindElement(By.Id("forma_cuentas_captacion_id_banco")));
            driver.FindElement(By.Id("forma_cuentas_captacion_id_banco")).Click();
            var opcionInstitucion = driver.FindElement(By.Id("forma_cuentas_captacion_id_banco"));
            var seleccionar_institucion = new SelectElement(opcionInstitucion);
            seleccionar_institucion.SelectByValue("14");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("forma_cuentas_captacion_tipo_inversion")).SendKeys("CHEQUES");
            driver.FindElement(By.Id("forma_cuentas_captacion_numero_contrato_tarjeta")).SendKeys("123443434343434343");
            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();
            System.Threading.Thread.Sleep(5000);
            */


            //REFERENCIAS PERSONALES DEL AVAL
            driver.FindElement(By.Id("forma_familiar_nombre_completo")).SendKeys("Guillermina Sámano Pérez");
            SelectElement parentesco_aval = new SelectElement(driver.FindElement(By.Id("forma_familiar_parentesco_tiempo_conocerlo")));
            driver.FindElement(By.Id("forma_familiar_parentesco_tiempo_conocerlo")).Click();
            var opcionParentesco = driver.FindElement(By.Id("forma_familiar_parentesco_tiempo_conocerlo"));
            var seleccionar_parentesco = new SelectElement(opcionParentesco);
            seleccionar_parentesco.SelectByValue("Madre");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_familiar_calle")).SendKeys("LAS PALMERAS");
            driver.FindElement(By.Id("forma_familiar_numero_exterior")).SendKeys("3434");
            driver.FindElement(By.Id("forma_familiar_colonia")).SendKeys("LA TUZANIA");

            SelectElement estado_referencia = new SelectElement(driver.FindElement(By.Id("familiar_id_municipio_estado")));
            driver.FindElement(By.Id("familiar_id_municipio_estado")).Click();
            var opcion_estado_referencia = driver.FindElement(By.Id("familiar_id_municipio_estado"));
            var seleccionar_estado_referencia = new SelectElement(opcion_estado_referencia);
            seleccionar_estado_referencia.SelectByValue("1");
            System.Threading.Thread.Sleep(5000);
            SelectElement municipio_referencia = new SelectElement(driver.FindElement(By.Id("familiar_id_municipio_id_municipio")));
            driver.FindElement(By.Id("familiar_id_municipio_id_municipio")).Click();
            var opcion_municipio_referencia = driver.FindElement(By.Id("familiar_id_municipio_id_municipio"));
            var seleccionar_municipio_referencia = new SelectElement(opcion_municipio_referencia);
            seleccionar_municipio_referencia.SelectByValue("43");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_familiar_telefono_codigo_ciudad")).SendKeys("23");
            driver.FindElement(By.Id("forma_familiar_telefono_numero")).SendKeys("34232323");

            //Referencia 2
            driver.FindElement(By.Id("forma_personal_nombre_completo")).SendKeys("Ricardo Morales");
            driver.FindElement(By.Id("forma_personal_parentesco_tiempo_conocerlo")).SendKeys("3");
            driver.FindElement(By.Id("forma_personal_calle")).SendKeys("LAS PALMERAS");
            driver.FindElement(By.Id("forma_personal_numero_exterior")).SendKeys("3434");
            driver.FindElement(By.Id("forma_personal_colonia")).SendKeys("LA TUZANIA");

            SelectElement estado_referencia2 = new SelectElement(driver.FindElement(By.Id("personal_id_municipio_estado")));
            driver.FindElement(By.Id("personal_id_municipio_estado")).Click();
            var opcion_estado_referencia2 = driver.FindElement(By.Id("personal_id_municipio_estado"));
            var seleccionar_estado_referencia2 = new SelectElement(opcion_estado_referencia2);
            seleccionar_estado_referencia2.SelectByValue("1");
            System.Threading.Thread.Sleep(5000);

            SelectElement municipio_referencia2 = new SelectElement(driver.FindElement(By.Id("personal_id_municipio_id_municipio")));
            driver.FindElement(By.Id("personal_id_municipio_id_municipio")).Click();
            var opcion_municipio_referencia2 = driver.FindElement(By.Id("personal_id_municipio_id_municipio"));
            var seleccionar_municipio_referencia2 = new SelectElement(opcion_municipio_referencia2);
            seleccionar_municipio_referencia2.SelectByValue("43");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("forma_personal_telefono_codigo_ciudad")).SendKeys("23");
            driver.FindElement(By.Id("forma_personal_telefono_numero")).SendKeys("34232323");

            //Referencia 3
            driver.FindElement(By.Id("forma_personal_secundario_nombre_completo")).SendKeys("Ricardo Morales");
            driver.FindElement(By.Id("forma_personal_secundario_parentesco_tiempo_conocerlo")).SendKeys("3");
            driver.FindElement(By.Id("forma_personal_secundario_calle")).SendKeys("LAS PALMERAS");
            driver.FindElement(By.Id("forma_personal_secundario_numero_exterior")).SendKeys("3434");
            driver.FindElement(By.Id("forma_personal_secundario_colonia")).SendKeys("LA TUZANIA");

            SelectElement estado_referencia3 = new SelectElement(driver.FindElement(By.Id("personal_secundario_id_municipio_estado")));
            driver.FindElement(By.Id("personal_secundario_id_municipio_estado")).Click();
            var opcion_estado_referencia3 = driver.FindElement(By.Id("personal_secundario_id_municipio_estado"));
            var seleccionar_estado_referencia3 = new SelectElement(opcion_estado_referencia3);
            seleccionar_estado_referencia3.SelectByValue("1");
            System.Threading.Thread.Sleep(5000);

            SelectElement municipio_referencia3 = new SelectElement(driver.FindElement(By.Id("personal_secundario_id_municipio_id_municipio")));
            driver.FindElement(By.Id("personal_secundario_id_municipio_id_municipio")).Click();
            var opcion_municipio_referencia3 = driver.FindElement(By.Id("personal_secundario_id_municipio_id_municipio"));
            var seleccionar_municipio_referencia3 = new SelectElement(opcion_municipio_referencia3);
            seleccionar_municipio_referencia3.SelectByValue("43");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("forma_personal_secundario_telefono_codigo_ciudad")).SendKeys("23");
            driver.FindElement(By.Id("forma_personal_secundario_telefono_numero")).SendKeys("34232323");
            System.Threading.Thread.Sleep(5000);

            this.NextClick(es_solicitante, "pm");

            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("finish_solicitante")).Click();

            //Identificar coincidencias

        }

        [Test]
        public void PersonaFisicaConActividadEmpresarialTest()
        {
            //PARAMETROS PARA PRUEBAS
            var nombre = "ALBERTO JOSÉ";
            var apellido_p = "GONZALEZ";
            var apellido_m = "Cortez";
            var rfc = "AEOJ810126665";
            var fecha = "1981-01-26";

            var es_solicitante = false;

            //Login
            if (Environment.GetEnvironmentVariable("tipo") == "solicitante")
            {
                driver.Navigate().GoToUrl("http://74.127.61.116/pruebas/arrendamas/solicitante2007/menu_solicitante.php");

                System.Threading.Thread.Sleep(5000);

                driver.FindElement(By.CssSelector(".personaFisicaEmpresarial")).Click();
                driver.FindElement(By.CssSelector("#div_progress > div:nth-child(1) > div:nth-child(1) > div:nth-child(5) > input:nth-child(1)")).Click();
                driver.FindElement(By.CssSelector("#wizard_pfae_botones > a:nth-child(2) > img:nth-child(1)")).Click();

                es_solicitante = true;
            }
            else
            {
                this._login();

                driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div/div[2]/div/div[2]/div[1]/a[2]")).Click();//PFAE
                driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div/div[2]/ul/li[2]/a")).Click();

            }

            //Bloque datos generales
            driver.FindElement(By.Id("forma_nombre")).SendKeys(nombre);
            driver.FindElement(By.Id("forma_apellido_paterno")).SendKeys(apellido_p);
            driver.FindElement(By.Id("forma_apellido_materno")).SendKeys(apellido_m);
            driver.FindElement(By.Id("forma_rfc")).SendKeys(rfc);
            driver.FindElement(By.Id("forma_email")).SendKeys("amartinez.jegra@gmail.com");
            driver.FindElement(By.Id("forma_celular_codigo_ciudad")).SendKeys("67");
            driver.FindElement(By.Id("forma_celular_numero")).SendKeys("89676759");

            //Seleccionar el género del cliente
            SelectElement genero = new SelectElement(driver.FindElement(By.Id("forma_sexo")));
            driver.FindElement(By.Id("forma_sexo")).Click();
            var sexo = driver.FindElement(By.Id("forma_sexo"));
            var selectElement = new SelectElement(sexo);
            //System.Threading.Thread.Sleep(1000);
            selectElement.SelectByText("Masculino");

            //Seleccionar la fecha de nacimiento
            driver.FindElement(By.Id("forma_fecha_nacimiento")).SendKeys(fecha);
            //Nacionalidad
            driver.FindElement(By.Id("forma_nacionalidad")).SendKeys("Mexicana");
            //Seleccionar Estado civil del cliente
            SelectElement estadoci = new SelectElement(driver.FindElement(By.Id("forma_estado_civil")));
            driver.FindElement(By.Id("forma_estado_civil")).Click();
            var estadoc = driver.FindElement(By.Id("forma_estado_civil"));
            var selectElement1 = new SelectElement(estadoc);
            //( System.Threading.Thread.Sleep(1000);
            selectElement1.SelectByValue("Soltero");

            //Seleccionar identificación
            SelectElement identificacion = new SelectElement(driver.FindElement(By.Id("forma_identificacion")));
            driver.FindElement(By.Id("forma_identificacion")).Click();
            var identi = driver.FindElement(By.Id("forma_identificacion"));
            var selectElement2 = new SelectElement(identi);
            //( System.Threading.Thread.Sleep(1000);
            selectElement2.SelectByValue("INE");

            //Seleccionar Grado máximo de estudios
            SelectElement GradoM = new SelectElement(driver.FindElement(By.Id("forma_grado_maximo_estudios")));
            driver.FindElement(By.Id("forma_grado_maximo_estudios")).Click();
            var grado = driver.FindElement(By.Id("forma_grado_maximo_estudios"));
            var selectGrado = new SelectElement(grado);
            //( System.Threading.Thread.Sleep(1000);
            selectGrado.SelectByValue("Ninguno");

            driver.FindElement(By.Id("forma_dependientes_economicos_numero")).SendKeys("0");
            this.NextClick(es_solicitante, "pfae");


            //BLOQUE INFORMACIÓN FISCAL
            //domicilio fiscal
            driver.FindElement(By.Id("forma_facturacion_calle")).SendKeys("San rafael");
            driver.FindElement(By.Id("forma_facturacion_numero_exterior")).SendKeys("2002");
            driver.FindElement(By.Id("forma_facturacion_numero_interior")).SendKeys("4");
            //Seleccionar país
            var opcionPais_IF = driver.FindElement(By.Id("forma_facturacion_id_pais"));
            var seleccionarPais2 = new SelectElement(opcionPais_IF);
            System.Threading.Thread.Sleep(4000);//Tiempo de espera
            seleccionarPais2.SelectByValue("435");
            //Código postal
            driver.FindElement(By.Id("forma_facturacion_codigo_postal")).SendKeys("45138");
            System.Threading.Thread.Sleep(4000);
            //Seleccionar colonia
            var opcionColonia2 = driver.FindElement(By.CssSelector("#forma_combo_facturacion_colonia"));
            var seleccionarColonia2 = new SelectElement(opcionColonia2);
            System.Threading.Thread.Sleep(4000);
            seleccionarColonia2.SelectByText("EL TIGRE");
            System.Threading.Thread.Sleep(4000);

            //Tomar datos de domicilio fiscal
            driver.FindElement(By.Id("copy_domicilio_fiscal")).Click();
            System.Threading.Thread.Sleep(4000);
            this.NextClick(es_solicitante, "pfae");

            //BLOQUE VIVIENDA
            SelectElement vivienda = new SelectElement(driver.FindElement(By.Id("forma_vivienda_tipo")));
            driver.FindElement(By.Id("forma_vivienda_tipo")).Click();
            var opcionvivienda = driver.FindElement(By.Id("forma_vivienda_tipo"));
            var seleccionarVivienda = new SelectElement(opcionvivienda);
            //( System.Threading.Thread.Sleep(1000);
            seleccionarVivienda.SelectByValue("Propia");
            driver.FindElement(By.Id("forma_vivienda_valor_aproximado")).SendKeys("10000");
            driver.FindElement(By.Id("forma_vivienda_anios_residencia")).SendKeys("2");
            driver.FindElement(By.Id("forma_vivienda_meses_residencia")).SendKeys("24");
            driver.FindElement(By.Id("forma_vivienda_codigo_ciudad")).SendKeys("67");
            driver.FindElement(By.Id("forma_vivienda_numero")).SendKeys("89676745");
            driver.FindElement(By.Id("forma_vivienda_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_vivienda_numero_exterior")).SendKeys("56");

            //Seleccionar país
            var opcionPais = driver.FindElement(By.Id("forma_vivienda_id_pais"));
            var seleccionarPais = new SelectElement(opcionPais);
            System.Threading.Thread.Sleep(4000);//Tiempo de espera
            seleccionarPais.SelectByValue("435");
            //Código postal
            driver.FindElement(By.Id("forma_vivienda_codigo_postal")).SendKeys("45138");
            //Seleccionar colonia
            var opcionColonia = driver.FindElement(By.Id("forma_combo_vivienda_colonia"));
            var seleccionarColonia = new SelectElement(opcionColonia);
            System.Threading.Thread.Sleep(4000);
            seleccionarColonia.SelectByText("EL TIGRE");
            System.Threading.Thread.Sleep(4000);
            this.NextClick(es_solicitante, "pfae");

            //BLOQUE EMPLEO
            driver.FindElement(By.Id("forma_profesion_actual")).SendKeys("Ingeniero");
            driver.FindElement(By.Id("forma_empresa_puesto_actual")).SendKeys("Gerente");
            var opcionPosicion = driver.FindElement(By.Id("forma_empresa_posicion"));
            var selectPosicion = new SelectElement(opcionPosicion);
            System.Threading.Thread.Sleep(5000);
            selectPosicion.SelectByText("Propietario");
            driver.FindElement(By.Id("forma_empresa_fecha_ingreso")).SendKeys("2011-01-21");
            driver.FindElement(By.Id("forma_empresa_nombre")).SendKeys("EMPRESA");
            var opcioncontrato = driver.FindElement(By.Id("forma_empresa_tipo_contrato"));
            var seleccionarcontrato = new SelectElement(opcioncontrato);
            System.Threading.Thread.Sleep(4000);
            seleccionarcontrato.SelectByText("Fijo");
            driver.FindElement(By.Id("forma_empresa_codigo_ciudad")).SendKeys("33");
            driver.FindElement(By.Id("forma_empresa_numero")).SendKeys("56341289");
            driver.FindElement(By.Id("forma_empresa_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_empresa_numero_exterior")).SendKeys("78");
            var opcionPaisEmpresa = driver.FindElement(By.Id("forma_empresa_id_pais"));
            var seleccionarpaisempresa = new SelectElement(opcionPaisEmpresa);
            System.Threading.Thread.Sleep(5000);
            seleccionarpaisempresa.SelectByValue("435");
            driver.FindElement(By.Id("forma_empresa_codigo_postal")).SendKeys("45138");
            System.Threading.Thread.Sleep(5000);
            var opcionColoniaEmpresa = driver.FindElement(By.Id("forma_combo_empresa_colonia"));
            var seleccionarcolonia = new SelectElement(opcionColoniaEmpresa);
            System.Threading.Thread.Sleep(5000);
            seleccionarcolonia.SelectByText("COLEGIO DEL AIRE");
            this.NextClick(es_solicitante, "pfae");

            //BLOQUE INGRESOS Y EGRESOS
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_ingresos_mensuales_fijos")).SendKeys("50000");
            driver.FindElement(By.Id("forma_egresos_gasto_familiar")).SendKeys("21000");
            this.NextClick(es_solicitante, "pfae");
            System.Threading.Thread.Sleep(5000);

            //BLOQUE ACTIVOS Y PASIVOS
            this.NextClick(es_solicitante, "pfae");
            System.Threading.Thread.Sleep(5000);

            //BLOQUE INFORMACIÓN BANCARIA
            /*
            var opcionInstitucion = driver.FindElement(By.Id("forma_cuentas_captacion_id_banco"));
            var seleccionarbanco = new SelectElement(opcionInstitucion);
            seleccionarbanco.SelectByValue("8");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_cuentas_captacion_tipo_inversion")).SendKeys("CHEQUES");
            driver.FindElement(By.Id("forma_cuentas_captacion_numero_contrato_tarjeta")).SendKeys("56341234567889098765");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[1]/div[1]/div[3]/div/div/div[4]/a[2]/img")).Click();
            System.Threading.Thread.Sleep(5000);
            */

            //BLOQUE REFERENCIAS PERSONALES
            driver.FindElement(By.Id("forma_familiar_nombre_completo")).SendKeys("RAFAELA DURAN");
            var opcionParentesco = driver.FindElement(By.Id("forma_familiar_parentesco_tiempo_conocerlo"));
            var seleccionarParentesco = new SelectElement(opcionParentesco);
            seleccionarParentesco.SelectByText("Padre");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_familiar_calle")).SendKeys("LAS PALMERAS");
            driver.FindElement(By.Id("forma_familiar_numero_exterior")).SendKeys("78");
            driver.FindElement(By.Id("forma_familiar_colonia")).SendKeys("LA TUZANIA");
            System.Threading.Thread.Sleep(5000);
            var estadoreferencia = driver.FindElement(By.Id("familiar_id_municipio_estado"));
            var seleccionarEstadoReferencia = new SelectElement(estadoreferencia);
            seleccionarEstadoReferencia.SelectByText("Aguascalientes");
            System.Threading.Thread.Sleep(5000);
            var ciudadreferencia = driver.FindElement(By.Id("familiar_id_municipio_id_municipio"));
            var seleccionarciudadreferencia = new SelectElement(ciudadreferencia);
            seleccionarciudadreferencia.SelectByText("Asientos");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_familiar_telefono_codigo_ciudad")).SendKeys("22");
            driver.FindElement(By.Id("forma_familiar_telefono_numero")).SendKeys("23322323");


            driver.FindElement(By.Id("forma_personal_nombre_completo")).SendKeys("RICARDO");
            driver.FindElement(By.Id("forma_personal_parentesco_tiempo_conocerlo")).SendKeys("4");
            driver.FindElement(By.Id("forma_personal_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_personal_numero_exterior")).SendKeys("89");
            driver.FindElement(By.Id("forma_personal_colonia")).SendKeys("La Tuzania");
            System.Threading.Thread.Sleep(5000);
            var estadoreferenciapersonal = driver.FindElement(By.Id("personal_id_municipio_estado"));
            var seleccionarEstadoReferenciapersonal = new SelectElement(estadoreferenciapersonal);
            seleccionarEstadoReferenciapersonal.SelectByText("Aguascalientes");
            System.Threading.Thread.Sleep(5000);
            var ciudadreferenciapersonal = driver.FindElement(By.Id("personal_id_municipio_id_municipio"));
            var seleccionarciudadreferenciapersonal = new SelectElement(ciudadreferenciapersonal);
            seleccionarciudadreferenciapersonal.SelectByText("Asientos");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_personal_telefono_codigo_ciudad")).SendKeys("45");
            driver.FindElement(By.Id("forma_personal_telefono_numero")).SendKeys("45342323");


            driver.FindElement(By.Id("forma_personal_secundario_nombre_completo")).SendKeys("Rita");
            driver.FindElement(By.Id("forma_personal_secundario_parentesco_tiempo_conocerlo")).SendKeys("2");
            driver.FindElement(By.Id("forma_personal_secundario_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_personal_secundario_numero_exterior")).SendKeys("2");
            driver.FindElement(By.Id("forma_personal_secundario_colonia")).SendKeys("La tuzania");
            System.Threading.Thread.Sleep(5000);
            var estadoreferenciapersonalsegundo = driver.FindElement(By.Id("personal_secundario_id_municipio_estado"));
            var seleccionarEstadoReferenciapersonalsegundo = new SelectElement(estadoreferenciapersonalsegundo);
            seleccionarEstadoReferenciapersonalsegundo.SelectByText("Aguascalientes");
            System.Threading.Thread.Sleep(5000);
            var ciudadreferenciapersonalsegundo = driver.FindElement(By.Id("personal_secundario_id_municipio_id_municipio"));
            var seleccionarciudadreferenciapersonalsegundo = new SelectElement(ciudadreferenciapersonalsegundo);
            seleccionarciudadreferenciapersonalsegundo.SelectByText("Asientos");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_personal_secundario_telefono_codigo_ciudad")).SendKeys("56");
            driver.FindElement(By.Id("forma_personal_secundario_telefono_numero")).SendKeys("25656565");

            this.NextClick(es_solicitante, "pfae");
            System.Threading.Thread.Sleep(5000);

            this.NextClick(es_solicitante, "pfae"); //en realidad es finalizar

              System.Threading.Thread.Sleep(5000);


        }

    }
}
