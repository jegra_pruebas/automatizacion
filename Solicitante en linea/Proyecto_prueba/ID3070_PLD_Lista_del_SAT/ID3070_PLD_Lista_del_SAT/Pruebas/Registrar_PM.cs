﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ID3070_PLD_Lista_del_SAT.Pruebas
{
    class Registrar_PM
    {
        IWebDriver driver;

        [SetUp]
        public void Initializate()
        {
            driver = new FirefoxDriver();
        }

        [Test]
        public void LoginTest()
        {
            var razon_social = "Aliazra Ra’ad Ahmad";            
            var RFC = "ACL110520PE7";
            var fecha = "2011-05-20";

            //Variables para configurar datos del representante legal
            var nombre_representante_legal = "HÉCTOR";
            var paterno_representante_legal = "ARMENTA";
            var materno_representante_legal = "BRACAMONTES";
            //Por cada ejecución hay que modificar el RFC y/o la fecha de constitución 
            var rfc_representante_legal = "";

            //Variables para configurar AVAL
            var nombre_aval = "AARÓN";
            var apellido_p_aval = "VERDUZCO";
            var apellido_m_aval = "López";
            var RFC_aval = "AEMG791122ML7";
            var fecha_aval = "1979-11-22";


            //Login
            driver.Navigate().GoToUrl("https://sistema.arrendamas.com/fake/arrendamas/index.php?salir=1");

            System.Threading.Thread.Sleep(5000);
            // driver.FindElement(By.Id("advancedButton")).Click();
            //driver.FindElement(By.Id("exceptionDialogButton")).Click();
            

            driver.FindElement(By.Id("login")).SendKeys("x.asoriano");
            driver.FindElement(By.Id("password")).SendKeys("aproduccion");
            driver.FindElement(By.ClassName("botonBloque")).Click();
            System.Threading.Thread.Sleep(5000);
            

            //Entrar a Admin clientes
            IWebElement EnterClientes = driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[2]/div[2]/div/div[2]/div/a[3]"));
            Actions action1 = new Actions(driver); //referencia para posicionarnos en el botón de crear solicitud
            action1.MoveToElement(EnterClientes).Perform();
            IWebElement ClickSolicitud = driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[2]/div[2]/div/div[2]/div/a[3]/span[2]/span[1]"));
            ClickSolicitud.Click();

            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[2]/ul[1]/li[2]/a")).Click();//Pestaña Agregar persona

            driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div/div[2]/div/div[2]/div[1]/a[3]")).Click();//PM
            driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div/div[2]/div/div[1]/div/div[2]/ul/li[2]/a")).Click();
            driver.FindElement(By.Id("forma_nombre")).SendKeys(razon_social);
            driver.FindElement(By.Id("forma_rfc")).SendKeys(RFC);

            //Seleccionar el sector del cliente
            SelectElement sector = new SelectElement(driver.FindElement(By.Id("forma_sector")));
            driver.FindElement(By.Id("forma_sector")).Click();
            var indu = driver.FindElement(By.Id("forma_sector"));
            var selectElement1 = new SelectElement(indu);
            System.Threading.Thread.Sleep(1000);
            selectElement1.SelectByText("Industria");


            //Seleccionar Actividad de la empresa
            /*SelectElement actividad = new SelectElement(driver.FindElement(By.Id("select2-id_empresa_actividad-container")));
            driver.FindElement(By.Id("select2-id_empresa_actividad-container")).Click();
            var acabado = driver.FindElement(By.Id("select2-id_empresa_actividad-container"));
            var selectElement_actividad = new SelectElement(acabado);
            System.Threading.Thread.Sleep(1000);
            selectElement_actividad.SelectByText("ACABADO DE HILOS");*/

            driver.FindElement(By.Id("select2-id_empresa_actividad-container")).Click();
            driver.FindElement(By.Id("select2 - search__field")).Click();
            driver.FindElement(By.Id("select2 - search__field")).SendKeys("ACABADO DE HILOS");
            driver.FindElement(By.Id("select2 - search__field")).Click();

            //Número de empleados
            driver.FindElement(By.Id("forma_numero_empleados")).SendKeys("2");

            driver.FindElement(By.Id("forma_telefono_codigo_ciudad")).SendKeys("238");
            driver.FindElement(By.Id("forma_telefono_numero")).SendKeys("8967675");

            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();
            //---
           
            //BLOQUE INFORMACIÓN FISCAL
            //domicilio fiscal
            driver.FindElement(By.Id("forma_facturacion_calle")).SendKeys("San rafael");
            driver.FindElement(By.Id("forma_facturacion_numero_exterior")).SendKeys("2002");
            driver.FindElement(By.Id("forma_facturacion_numero_interior")).SendKeys("4");
            //Seleccionar país
            var opcionPais_IF = driver.FindElement(By.Id("forma_facturacion_id_pais"));
            var seleccionarPais2 = new SelectElement(opcionPais_IF);
            System.Threading.Thread.Sleep(4000);//Tiempo de espera
            seleccionarPais2.SelectByText("MÉXICO");
            //Código postal
            driver.FindElement(By.Id("forma_facturacion_codigo_postal")).SendKeys("45138");
            System.Threading.Thread.Sleep(4000);
            //Seleccionar colonia
            var opcionColonia2 = driver.FindElement(By.CssSelector("#forma_combo_facturacion_colonia"));
            var seleccionarColonia2 = new SelectElement(opcionColonia2);
            System.Threading.Thread.Sleep(4000);
            seleccionarColonia2.SelectByText("EL TIGRE");
            //Tomar datos de domicilio fiscal
            driver.FindElement(By.Id("copy_domicilio_fiscal")).Click();
            System.Threading.Thread.Sleep(4000);

            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();

            //BLOQUE INFORMACIÓN DE ESCRITURA
            driver.FindElement(By.Id("forma_fecha_constitucion")).SendKeys(fecha);
            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();

            //BLOQUE CONTACTOS
            //Representante legal
            driver.FindElement(By.Id("forma_representante_legal_nombre")).SendKeys(nombre_representante_legal);
            driver.FindElement(By.Id("forma_representante_legal_apellido_paterno")).SendKeys(paterno_representante_legal);
            driver.FindElement(By.Id("forma_representante_legal_apellido_materno")).SendKeys(materno_representante_legal);
            driver.FindElement(By.Id("forma_representante_legal_email")).SendKeys("isoto.jegra@gmail.com");
            driver.FindElement(By.Id("forma_representante_legal_rfc")).SendKeys(rfc_representante_legal);
            driver.FindElement(By.Id("forma_representante_legal_puesto")).SendKeys("GERENTE");
            driver.FindElement(By.Id("forma_representante_legal_codigo_ciudad")).SendKeys("22");
            driver.FindElement(By.Id("forma_representante_legal_telefono")).SendKeys("23322323");

            //Contacto para gestionar este contrato
            driver.FindElement(By.Id("check_copy_from_contrato")).Click();
            System.Threading.Thread.Sleep(4000);

            //Contacto de pago
            driver.FindElement(By.Id("check_copy_pago")).Click();
            System.Threading.Thread.Sleep(4000);

            //Administrador general
            driver.FindElement(By.Id("check_copy_administrador")).Click();
            System.Threading.Thread.Sleep(4000);

            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();


            //BLOQUE PODERES
            driver.FindElement(By.Id("forma_0_nombre")).SendKeys("Rosa");
            driver.FindElement(By.Id("forma_0_apellido_paterno")).SendKeys("Rosales");

            driver.FindElement(By.Id("forma_0_poder_1")).Click();
            driver.FindElement(By.Id("forma_0_poder_2")).Click();
            driver.FindElement(By.Id("forma_0_poder_3")).Click();

            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();

            //BLOQUE ACCIONISTA MAYORITARIO
            //Seleccionar el tipo de persona
            SelectElement tipo_persona = new SelectElement(driver.FindElement(By.Id("forma_0_tipo_persona")));
            driver.FindElement(By.Id("forma_0_tipo_persona")).Click();
            var opcionPM = driver.FindElement(By.Id("forma_0_tipo_persona"));
            var seleccionar_tipo_persona = new SelectElement(opcionPM);
            //( System.Threading.Thread.Sleep(1000);
            seleccionar_tipo_persona.SelectByValue("Moral");
            //compañia
            driver.FindElement(By.Id("forma_0_nombre")).SendKeys("JEGRA");
            driver.FindElement(By.Id("forma_0_rfc")).SendKeys("MASA890925mp");
            //Porcentaje
            driver.FindElement(By.Id("forma_0_porcentaje_participacion")).SendKeys("100");

            driver.FindElement(By.Id("forma_0_accionista_calle")).SendKeys("San rafael");
            driver.FindElement(By.Id("forma_0_accionista_numero_exterior")).SendKeys("12");
            driver.FindElement(By.Id("forma_0_accionista_numero_interior")).SendKeys("10");
            //Código postal y colonia
            driver.FindElement(By.Id("forma_0_accionista_codigo_postal")).SendKeys("45138");
            System.Threading.Thread.Sleep(5000);
            SelectElement colonia = new SelectElement(driver.FindElement(By.Id("forma_0_combo_accionista_colonia")));
            driver.FindElement(By.Id("forma_0_combo_accionista_colonia")).Click();
            var opcionAccionistaColonia = driver.FindElement(By.Id("forma_0_combo_accionista_colonia"));
            var seleccionar_colonia = new SelectElement(opcionAccionistaColonia);
            seleccionar_colonia.SelectByValue("EL TIGRE");

            
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();

            //BLOQUE CUENTAS DE CAPTACIÓN
            /*Se oculpta por ID3097
            System.Threading.Thread.Sleep(5000);
            SelectElement banco = new SelectElement(driver.FindElement(By.Id("forma_0_cuentas_captacion_id_banco")));
            driver.FindElement(By.Id("forma_0_cuentas_captacion_id_banco")).Click();
            var opcionBanco = driver.FindElement(By.Id("forma_0_cuentas_captacion_id_banco"));
            var seleccionar_banco = new SelectElement(opcionBanco);
            seleccionar_banco.SelectByValue("14");
            System.Threading.Thread.Sleep(5000);
            

            driver.FindElement(By.Id("forma_0_cuentas_captacion_tipo_inversion")).SendKeys("CHEQUES");
            driver.FindElement(By.Id("forma_0_cuentas_captacion_clabe")).SendKeys("123443434343434343");
            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();
            System.Threading.Thread.Sleep(5000);
            */

            System.Threading.Thread.Sleep(5000);
            //BLOQUE REFERENCIAS(PROVEEDORES)
            driver.FindElement(By.CssSelector("#forma_0_nombre_completo")).SendKeys("LA ECONOMICA");
            driver.FindElement(By.Id("forma_0_nombre_contacto")).SendKeys("Luis Rivera");
            driver.FindElement(By.Id("forma_0_contacto_puesto")).SendKeys("Director");
            driver.FindElement(By.Id("forma_0_telefono_codigo_ciudad")).SendKeys("33");
            driver.FindElement(By.Id("forma_0_telefono_numero")).SendKeys("56341213");

            driver.FindElement(By.Id("forma_1_nombre_completo")).SendKeys("LA SURTIDA");
            driver.FindElement(By.Id("forma_1_nombre_contacto")).SendKeys("Pedro Rivera");
            driver.FindElement(By.Id("forma_1_contacto_puesto")).SendKeys("Gerente");
            driver.FindElement(By.Id("forma_1_telefono_codigo_ciudad")).SendKeys("33");
            driver.FindElement(By.Id("forma_1_telefono_numero")).SendKeys("56341213");
            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();
            System.Threading.Thread.Sleep(5000);


            //BLOQUE INFORMACIÓN FINANCIERA
            //Seleccionar período de ventas
            SelectElement periodoVentas = new SelectElement(driver.FindElement(By.Id("forma_ventas_periodo")));
            driver.FindElement(By.Id("forma_ventas_periodo")).Click();
            var opcionperiodo = driver.FindElement(By.Id("forma_ventas_periodo"));
            var seleccionar_periodo = new SelectElement(opcionperiodo);
            seleccionar_periodo.SelectByValue("Todo el año");
            System.Threading.Thread.Sleep(5000);
            //Seleccionar sistema de ventas
            SelectElement sistemaVentas = new SelectElement(driver.FindElement(By.Id("forma_ventas_sistema")));
            driver.FindElement(By.Id("forma_ventas_sistema")).Click();
            var opcionsistema = driver.FindElement(By.Id("forma_ventas_sistema"));
            var seleccionar_sistema = new SelectElement(opcionsistema);
            seleccionar_sistema.SelectByValue("Mayoreo");
        
            driver.FindElement(By.Id("forma_ventas_porcentaje_fabricacion")).SendKeys("12");
            driver.FindElement(By.Id("forma_ventas_porcentaje_compras")).SendKeys("12");

            driver.FindElement(By.Id("forma_estados_financieros_fecha_inicio")).SendKeys("2018-11-02");
            driver.FindElement(By.Id("forma_estados_financieros_fecha_fin")).SendKeys("2018-11-02");
            driver.FindElement(By.Id("forma_monto_capital_social")).SendKeys("34353");
            driver.FindElement(By.Id("forma_monto_utilidades_acumuladas")).SendKeys("213123");
            driver.FindElement(By.Id("forma_monto_ventas_totales")).SendKeys("234234");
            driver.FindElement(By.Id("forma_monto_activo_circulante")).SendKeys("45645");
            driver.FindElement(By.Id("forma_monto_activo_total")).SendKeys("534352");
            driver.FindElement(By.Id("forma_monto_pasivo_circulante")).SendKeys("526263");
            driver.FindElement(By.Id("forma_monto_pasivo_total")).SendKeys("536362");
            driver.FindElement(By.Id("forma_monto_capital_contable")).SendKeys("32422");

            driver.FindElement(By.Id("forma_anterior_estados_financieros_fecha_inicio")).SendKeys("2018-11-02");
            driver.FindElement(By.Id("forma_anterior_estados_financieros_fecha_fin")).SendKeys("2018-11-02");
            driver.FindElement(By.Id("forma_anterior_monto_capital_social")).SendKeys("34353");
            driver.FindElement(By.Id("forma_anterior_monto_utilidades_acumuladas")).SendKeys("213123");
            driver.FindElement(By.Id("forma_anterior_monto_ventas_totales")).SendKeys("234234");
            driver.FindElement(By.Id("forma_anterior_monto_activo_circulante")).SendKeys("45645");
            driver.FindElement(By.Id("forma_anterior_monto_activo_total")).SendKeys("534352");
            driver.FindElement(By.Id("forma_anterior_monto_pasivo_circulante")).SendKeys("526263");
            driver.FindElement(By.Id("forma_anterior_monto_pasivo_total")).SendKeys("536362");
            driver.FindElement(By.Id("forma_anterior_monto_capital_contable")).SendKeys("32422");
            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();
            System.Threading.Thread.Sleep(5000);

            //BLOQUE AVAL
            driver.FindElement(By.Id("forma_nombre")).SendKeys(nombre_aval);
            driver.FindElement(By.Id("forma_apellido_paterno")).SendKeys(apellido_p_aval);
            driver.FindElement(By.Id("forma_apellido_materno")).SendKeys(apellido_m_aval);
            driver.FindElement(By.Id("forma_rfc")).SendKeys(RFC_aval);
            driver.FindElement(By.Id("forma_email")).SendKeys("isoto.jegra@gmail.com");
            driver.FindElement(By.Id("forma_celular_codigo_ciudad")).SendKeys("33");
            driver.FindElement(By.Id("forma_celular_numero")).SendKeys("34343434");

            SelectElement generoaval = new SelectElement(driver.FindElement(By.Id("forma_sexo")));
            driver.FindElement(By.Id("forma_sexo")).Click();
            var opciongenero = driver.FindElement(By.Id("forma_sexo"));
            var seleccionar_genero = new SelectElement(opciongenero);
            seleccionar_genero.SelectByValue("Masculino");

            driver.FindElement(By.Id("forma_fecha_nacimiento")).SendKeys(fecha_aval);

            SelectElement estadocivilaval = new SelectElement(driver.FindElement(By.Id("forma_estado_civil")));
            driver.FindElement(By.Id("forma_estado_civil")).Click();
            var opcionestadocivil = driver.FindElement(By.Id("forma_estado_civil"));
            var seleccionar_estadocivil = new SelectElement(opcionestadocivil);
            seleccionar_estadocivil.SelectByValue("Soltero");
            System.Threading.Thread.Sleep(5000);
            SelectElement identificacionaval = new SelectElement(driver.FindElement(By.Id("forma_identificacion")));
            driver.FindElement(By.Id("forma_identificacion")).Click();
            var opcionesidentificacion = driver.FindElement(By.Id("forma_identificacion"));
            var seleccionar_identificacion = new SelectElement(opcionesidentificacion);
            seleccionar_identificacion.SelectByValue("INE");
            System.Threading.Thread.Sleep(5000);
            SelectElement gradoestudios = new SelectElement(driver.FindElement(By.Id("forma_grado_maximo_estudios")));
            driver.FindElement(By.Id("forma_grado_maximo_estudios")).Click();
            var opcionesgrado = driver.FindElement(By.Id("forma_grado_maximo_estudios"));
            var seleccionar_grado = new SelectElement(opcionesgrado);
            seleccionar_grado.SelectByValue("Secundaria");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("forma_dependientes_economicos_numero")).SendKeys("0");
            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();


            //BLOQUE VIVIENDA
            SelectElement vivienda = new SelectElement(driver.FindElement(By.Id("forma_vivienda_tipo")));
            driver.FindElement(By.Id("forma_vivienda_tipo")).Click();
            var opcionvivienda = driver.FindElement(By.Id("forma_vivienda_tipo"));
            var seleccionarVivienda = new SelectElement(opcionvivienda);
            //( System.Threading.Thread.Sleep(1000);
            seleccionarVivienda.SelectByValue("Propia");
            driver.FindElement(By.Id("forma_vivienda_valor_aproximado")).SendKeys("10000");
            driver.FindElement(By.Id("forma_vivienda_anios_residencia")).SendKeys("2");
            driver.FindElement(By.Id("forma_vivienda_meses_residencia")).SendKeys("24");
            driver.FindElement(By.Id("forma_vivienda_codigo_ciudad")).SendKeys("67");
            driver.FindElement(By.Id("forma_vivienda_numero")).SendKeys("89676745");
            driver.FindElement(By.Id("forma_vivienda_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_vivienda_numero_exterior")).SendKeys("56");
            //Seleccionar país
            var opcionPais = driver.FindElement(By.Id("forma_vivienda_id_pais"));
            var seleccionarPais = new SelectElement(opcionPais);
            System.Threading.Thread.Sleep(4000);//Tiempo de espera
            seleccionarPais.SelectByText("MÉXICO");
            //Código postal
            driver.FindElement(By.Id("forma_vivienda_codigo_postal")).SendKeys("45138");
            //Seleccionar colonia
            var opcionColonia = driver.FindElement(By.Id("forma_combo_vivienda_colonia"));
            var seleccionarColonia = new SelectElement(opcionColonia);
            System.Threading.Thread.Sleep(4000);
            seleccionarColonia.SelectByText("EL TIGRE");
            driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div/div[2]/div/div[2]/div/div[2]/div[4]/a[2]/img")).Click();

            //BLOQUE EMPLEO
            driver.FindElement(By.Id("forma_profesion_actual")).SendKeys("Ingeniero");
            driver.FindElement(By.Id("forma_empresa_puesto_actual")).SendKeys("Gerente");
            driver.FindElement(By.Id("forma_empresa_fecha_ingreso")).SendKeys("2018-11-05");
            driver.FindElement(By.Id("forma_empresa_nombre")).SendKeys("Servicios de limpieza");

            SelectElement tipocontrato = new SelectElement(driver.FindElement(By.Id("forma_empresa_tipo_contrato")));
            driver.FindElement(By.Id("forma_empresa_tipo_contrato")).Click();
            var opcionescontrato = driver.FindElement(By.Id("forma_empresa_tipo_contrato"));
            var seleccionar_contrato = new SelectElement(opcionescontrato);
            seleccionar_contrato.SelectByValue("Fijo");
            System.Threading.Thread.Sleep(5000);

            SelectElement tipoempresa = new SelectElement(driver.FindElement(By.Id("forma_empresa_tipo")));
            driver.FindElement(By.Id("forma_empresa_tipo")).Click();
            var opcionesempresa = driver.FindElement(By.Id("forma_empresa_tipo"));
            var seleccionar_empresa = new SelectElement(opcionesempresa);
            seleccionar_empresa.SelectByValue("Pública");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("forma_empresa_codigo_ciudad")).SendKeys("33");
            driver.FindElement(By.Id("forma_empresa_numero")).SendKeys("56341289");
            driver.FindElement(By.Id("forma_empresa_calle")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("forma_empresa_numero_exterior")).SendKeys("78");

            SelectElement pais_aval = new SelectElement(driver.FindElement(By.Id("forma_empresa_id_pais")));
            driver.FindElement(By.Id("forma_empresa_id_pais")).Click();
            var opcionespais = driver.FindElement(By.Id("forma_empresa_id_pais"));
            var seleccionar_pais = new SelectElement(opcionespais);
            seleccionar_pais.SelectByValue("435");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("forma_empresa_codigo_postal")).SendKeys("45138");
            System.Threading.Thread.Sleep(5000);
            SelectElement colonia_aval = new SelectElement(driver.FindElement(By.Id("forma_combo_empresa_colonia")));
            var opcionColoniaEmpresa = driver.FindElement(By.Id("forma_combo_empresa_colonia"));
            var seleccionarcolonia = new SelectElement(opcionColoniaEmpresa);
            System.Threading.Thread.Sleep(5000);
            seleccionarcolonia.SelectByText("COLEGIO DEL AIRE");
            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();



            //BLOQUE INGRESOS Y EGRESOS
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_ingresos_mensuales_fijos")).SendKeys("50000");
            driver.FindElement(By.Id("forma_egresos_gasto_familiar")).SendKeys("21000");
            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();
            System.Threading.Thread.Sleep(5000);

            //BLOQUE ACTIVOS Y PASIVOS
            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();
            System.Threading.Thread.Sleep(5000);

            //BLOQUE INFORMACIÓN BANCARIA
            /*
            System.Threading.Thread.Sleep(5000);
            SelectElement institucion = new SelectElement(driver.FindElement(By.Id("forma_cuentas_captacion_id_banco")));
            driver.FindElement(By.Id("forma_cuentas_captacion_id_banco")).Click();
            var opcionInstitucion = driver.FindElement(By.Id("forma_cuentas_captacion_id_banco"));
            var seleccionar_institucion = new SelectElement(opcionInstitucion);
            seleccionar_institucion.SelectByValue("14");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("forma_cuentas_captacion_tipo_inversion")).SendKeys("CHEQUES");
            driver.FindElement(By.Id("forma_cuentas_captacion_numero_contrato_tarjeta")).SendKeys("123443434343434343");
            driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();
            System.Threading.Thread.Sleep(5000);
            */


            //REFERENCIAS PERSONALES DEL AVAL
            driver.FindElement(By.Id("forma_familiar_nombre_completo")).SendKeys("Guillermina Sámano Pérez");
            SelectElement parentesco_aval = new SelectElement(driver.FindElement(By.Id("forma_familiar_parentesco_tiempo_conocerlo")));
            driver.FindElement(By.Id("forma_familiar_parentesco_tiempo_conocerlo")).Click();
            var opcionParentesco = driver.FindElement(By.Id("forma_familiar_parentesco_tiempo_conocerlo"));
            var seleccionar_parentesco = new SelectElement(opcionParentesco);
            seleccionar_parentesco.SelectByValue("Madre");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_familiar_calle")).SendKeys("LAS PALMERAS");
            driver.FindElement(By.Id("forma_familiar_numero_exterior")).SendKeys("3434");
            driver.FindElement(By.Id("forma_familiar_colonia")).SendKeys("LA TUZANIA");

            SelectElement estado_referencia = new SelectElement(driver.FindElement(By.Id("familiar_id_municipio_estado")));
            driver.FindElement(By.Id("familiar_id_municipio_estado")).Click();
            var opcion_estado_referencia = driver.FindElement(By.Id("familiar_id_municipio_estado"));
            var seleccionar_estado_referencia = new SelectElement(opcion_estado_referencia);
            seleccionar_estado_referencia.SelectByValue("1");
            System.Threading.Thread.Sleep(5000);
            SelectElement municipio_referencia = new SelectElement(driver.FindElement(By.Id("familiar_id_municipio_id_municipio")));
            driver.FindElement(By.Id("familiar_id_municipio_id_municipio")).Click();
            var opcion_municipio_referencia = driver.FindElement(By.Id("familiar_id_municipio_id_municipio"));
            var seleccionar_municipio_referencia = new SelectElement(opcion_municipio_referencia);
            seleccionar_municipio_referencia.SelectByValue("43");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("forma_familiar_telefono_codigo_ciudad")).SendKeys("23");
            driver.FindElement(By.Id("forma_familiar_telefono_numero")).SendKeys("34232323");

            //Referencia 2
            driver.FindElement(By.Id("forma_personal_nombre_completo")).SendKeys("Ricardo Morales");
            driver.FindElement(By.Id("forma_personal_parentesco_tiempo_conocerlo")).SendKeys("3");
            driver.FindElement(By.Id("forma_personal_calle")).SendKeys("LAS PALMERAS");
            driver.FindElement(By.Id("forma_personal_numero_exterior")).SendKeys("3434");
            driver.FindElement(By.Id("forma_personal_colonia")).SendKeys("LA TUZANIA");

            SelectElement estado_referencia2 = new SelectElement(driver.FindElement(By.Id("personal_id_municipio_estado")));
            driver.FindElement(By.Id("personal_id_municipio_estado")).Click();
            var opcion_estado_referencia2 = driver.FindElement(By.Id("personal_id_municipio_estado"));
            var seleccionar_estado_referencia2 = new SelectElement(opcion_estado_referencia2);
            seleccionar_estado_referencia2.SelectByValue("1");
            System.Threading.Thread.Sleep(5000);

            SelectElement municipio_referencia2 = new SelectElement(driver.FindElement(By.Id("personal_id_municipio_id_municipio")));
            driver.FindElement(By.Id("personal_id_municipio_id_municipio")).Click();
            var opcion_municipio_referencia2 = driver.FindElement(By.Id("personal_id_municipio_id_municipio"));
            var seleccionar_municipio_referencia2 = new SelectElement(opcion_municipio_referencia2);
            seleccionar_municipio_referencia2.SelectByValue("43");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("forma_personal_telefono_codigo_ciudad")).SendKeys("23");
            driver.FindElement(By.Id("forma_personal_telefono_numero")).SendKeys("34232323");

            //Referencia 3
            driver.FindElement(By.Id("forma_personal_secundario_nombre_completo")).SendKeys("Ricardo Morales");
            driver.FindElement(By.Id("forma_personal_secundario_parentesco_tiempo_conocerlo")).SendKeys("3");
            driver.FindElement(By.Id("forma_personal_secundario_calle")).SendKeys("LAS PALMERAS");
            driver.FindElement(By.Id("forma_personal_secundario_numero_exterior")).SendKeys("3434");
            driver.FindElement(By.Id("forma_personal_secundario_colonia")).SendKeys("LA TUZANIA");

            SelectElement estado_referencia3 = new SelectElement(driver.FindElement(By.Id("personal_secundario_id_municipio_estado")));
            driver.FindElement(By.Id("personal_secundario_id_municipio_estado")).Click();
            var opcion_estado_referencia3 = driver.FindElement(By.Id("personal_secundario_id_municipio_estado"));
            var seleccionar_estado_referencia3 = new SelectElement(opcion_estado_referencia3);
            seleccionar_estado_referencia3.SelectByValue("1");
            System.Threading.Thread.Sleep(5000);

            SelectElement municipio_referencia3 = new SelectElement(driver.FindElement(By.Id("personal_secundario_id_municipio_id_municipio")));
            driver.FindElement(By.Id("personal_secundario_id_municipio_id_municipio")).Click();
            var opcion_municipio_referencia3 = driver.FindElement(By.Id("personal_secundario_id_municipio_id_municipio"));
            var seleccionar_municipio_referencia3 = new SelectElement(opcion_municipio_referencia3);
            seleccionar_municipio_referencia3.SelectByValue("43");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("forma_personal_secundario_telefono_codigo_ciudad")).SendKeys("23");
            driver.FindElement(By.Id("forma_personal_secundario_telefono_numero")).SendKeys("34232323");
            System.Threading.Thread.Sleep(5000);
            //driver.FindElement(By.CssSelector("#wizard_pm_botones > a:nth-child(2) > img:nth-child(1)")).Click();
            
            //Identificar coincidencias
        

        }

      
    }
}
