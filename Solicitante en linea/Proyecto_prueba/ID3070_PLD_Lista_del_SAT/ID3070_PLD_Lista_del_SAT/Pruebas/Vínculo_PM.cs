﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ID3070_PLD_Lista_del_SAT.Pruebas
{
    class Vinculo_PM
    {
        IWebDriver driver;

        [SetUp]
        public void Initializate()
        {
            driver = new FirefoxDriver();
        }

        [Test]
        public void LoginTest()
        {
            //Parametros iniciales
            //Variable búsqueda nombre
            var RFC = "ACE121002N40";
            //Variable vínculo patrimonial
            var conquien = "ROSALIA ROSAS ROSAS";

            //Login
            driver.Navigate().GoToUrl("http://sistema.arrendamas.com/pruebas/arrendamas/index.php?salir=1");
            driver.FindElement(By.Id("login")).SendKeys("x.asoriano");
            driver.FindElement(By.Id("password")).SendKeys("apruebas");
            driver.FindElement(By.ClassName("botonBloque")).Click();
            System.Threading.Thread.Sleep(5000);

            //Entrar a Admin clientes
            IWebElement EnterClientes = driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[2]/div[2]/div/div[2]/div/a[3]"));
            Actions action1 = new Actions(driver); //referencia para posicionarnos en el botón de crear solicitud
            action1.MoveToElement(EnterClientes).Perform();
            IWebElement ClickSolicitud = driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[2]/div[2]/div/div[2]/div/a[3]/span[2]/span[1]"));
            ClickSolicitud.Click();
            System.Threading.Thread.Sleep(5000);



            //Buscar al cliente
            driver.FindElement(By.CssSelector("input.campoTexto")).SendKeys(RFC);

            //Seleccionar campo búsqueda
            SelectElement nombre_busqueda = new SelectElement(driver.FindElement(By.Id("campo_busqueda")));
            driver.FindElement(By.Id("campo_busqueda")).Click();
            var busqueda_seleccionar = driver.FindElement(By.Id("campo_busqueda"));
            var selectElement_busqueda = new SelectElement(busqueda_seleccionar);
            selectElement_busqueda.SelectByValue("rfc");

            //Clic en buscar
            driver.FindElement(By.CssSelector(".botonBloque")).Click();

            System.Threading.Thread.Sleep(5000);

            //Entrar en a la entrevista PM
            driver.FindElement(By.CssSelector(".renglon-non-click > td:nth-child(8) > a:nth-child(6) > img:nth-child(1)")).Click();

            //Seleccionar operaciones internacionales
            SelectElement operaciones_internacionales = new SelectElement(driver.FindElement(By.Id("operaciones_internacionales")));
            driver.FindElement(By.Id("operaciones_internacionales")).Click();
            var operaciones_seleccionar = driver.FindElement(By.Id("operaciones_internacionales"));
            var selectElement_operaciones = new SelectElement(operaciones_seleccionar);
            System.Threading.Thread.Sleep(3000);
            selectElement_operaciones.SelectByValue("INVERSIONES");

            //Seleccionar país
            SelectElement pais = new SelectElement(driver.FindElement(By.Id("id_pais")));
            driver.FindElement(By.Id("id_pais")).Click();
            var pais_seleccionar = driver.FindElement(By.Id("id_pais"));
            var selectElement_pais = new SelectElement(pais_seleccionar);
            System.Threading.Thread.Sleep(3000);
            selectElement_pais.SelectByValue("269");

            //Seleccionar actividad economica
            SelectElement actividad = new SelectElement(driver.FindElement(By.Id("id_empresa_actividad")));
            driver.FindElement(By.Id("id_empresa_actividad")).Click();
            var actividad_seleccionar = driver.FindElement(By.Id("id_empresa_actividad"));
            var selectElement_actividad = new SelectElement(actividad_seleccionar);
            System.Threading.Thread.Sleep(3000);
            selectElement_actividad.SelectByValue("232");

            //Seleccionar sector
            SelectElement sector = new SelectElement(driver.FindElement(By.Id("id_sector")));
            driver.FindElement(By.Id("id_sector")).Click();
            var sector_seleccionar = driver.FindElement(By.Id("id_sector"));
            var selectElement_sector = new SelectElement(sector_seleccionar);
            System.Threading.Thread.Sleep(3000);
            selectElement_sector.SelectByValue("4");

            //Seleccionar industria
            SelectElement industria = new SelectElement(driver.FindElement(By.Id("id_industria")));
            driver.FindElement(By.Id("id_industria")).Click();
            var industria_seleccionar = driver.FindElement(By.Id("id_industria"));
            var selectElement_industria = new SelectElement(industria_seleccionar);
            System.Threading.Thread.Sleep(3000);
            selectElement_industria.SelectByValue("25");

            //Seleccionar sociedad
            SelectElement sociedad = new SelectElement(driver.FindElement(By.Id("id_pld_tipo_sociedad")));
            driver.FindElement(By.Id("id_pld_tipo_sociedad")).Click();
            var sociedad_seleccionar = driver.FindElement(By.Id("id_pld_tipo_sociedad"));
            var selectElement_sociedad = new SelectElement(sociedad_seleccionar);
            System.Threading.Thread.Sleep(3000);
            selectElement_sociedad.SelectByValue("9");

            //Fiel
            driver.FindElement(By.Id("fiel")).SendKeys("12345678");

            //Mantiene vínculo patrimonial
            SelectElement mantiene_vinculo = new SelectElement(driver.FindElement(By.Id("mantiene_vinculo_patrimonial")));
            driver.FindElement(By.Id("mantiene_vinculo_patrimonial")).Click();
            var vinculo_seleccionar = driver.FindElement(By.Id("mantiene_vinculo_patrimonial"));
            var selectElement_vinculo = new SelectElement(vinculo_seleccionar);
            System.Threading.Thread.Sleep(3000);
            selectElement_vinculo.SelectByValue("Si");

            driver.FindElement(By.Id("involucrado_vinculo_patrimonial")).SendKeys(conquien);

            driver.FindElement(By.Id("boton_guardar_datos")).Click();
        }
    }
}