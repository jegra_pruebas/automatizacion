﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;

namespace ID3070_PLD_Lista_del_SAT.Pruebas
{
    class Registrar_Garante_PM
    {
        IWebDriver driver;

        [SetUp]
        public void Initializate(
            )
        {
            driver = new FirefoxDriver();
        }

        [Test]
        public void RegistrarGarantePM()
        {
            driver.Navigate().GoToUrl("http://sistema.arrendamas.com/pruebas/arrendamas/index.php?salir=1");
            driver.FindElement(By.Id("login")).SendKeys("ines.soto");
            driver.FindElement(By.Id("password")).SendKeys("pruebas");
            driver.FindElement(By.ClassName("botonBloque")).Click();
            System.Threading.Thread.Sleep(5000);

            driver.Navigate().GoToUrl("http://sistema.arrendamas.com/pruebas/patron/modulos/ejecucion_workflow/editar_forma_estatus.php?id_workflow=1&id_wf_estatus=86&id_solicitud=6633&ver=1");
            System.Threading.Thread.Sleep(5000);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("window.scrollBy(0,2000)");

            driver.FindElement(By.CssSelector("a.ligaRegistro:nth-child(7)")).Click();
            driver.FindElement(By.CssSelector(".botonBloque")).Click();
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("pm_involucrado")).Click();
            driver.FindElement(By.Id("nombre")).SendKeys("La muy surtida");
            //driver.FindElement(By.Id("fecha_constitucion")).SendKeys("2018-11-06");
            IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
            jse.ExecuteScript("document.getElementById('fecha_constitucion').value='2018-01-01'");


            SelectElement pais = new SelectElement(driver.FindElement(By.Id("id_pais")));
            driver.FindElement(By.Id("id_pais")).Click();
            var opcionpais = driver.FindElement(By.Id("id_pais"));
            var seleccionar_pais = new SelectElement(opcionpais);
            seleccionar_pais.SelectByValue("435");

            SelectElement actividad = new SelectElement(driver.FindElement(By.Id("id_empresa_actividad")));
            driver.FindElement(By.Id("id_empresa_actividad")).Click();
            var opcionactividad = driver.FindElement(By.Id("id_empresa_actividad"));
            var seleccionar_actividad = new SelectElement(opcionactividad);
            seleccionar_actividad.SelectByValue("6");
            System.Threading.Thread.Sleep(2000);

            driver.FindElement(By.Id("nombre_representante_legal")).SendKeys("Juan Manuel Cordoba Mendez");
            driver.FindElement(By.Id("email_representante_legal")).SendKeys("ISOTO.JEGRA@GMAIL.COM");
            driver.FindElement(By.Id("calle_fiscal")).SendKeys("Las palmeras");
            driver.FindElement(By.Id("numero_exterior_fiscal")).SendKeys("45");
            driver.FindElement(By.Id("codigo_postal_fiscal")).SendKeys("45138");
            System.Threading.Thread.Sleep(5000);

            driver.FindElement(By.Id("codigo_postal_empresa")).SendKeys("45138");
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("numero_interior_empresa")).Click();
            System.Threading.Thread.Sleep(5000);
            driver.FindElement(By.Id("validarGarante")).Click();
            System.Threading.Thread.Sleep(5000);
            //Este es el botón para después de que se encuentre la coincidencia
            driver.FindElement(By.Id("boton_submit_forma")).Click();


        }

    }
}
