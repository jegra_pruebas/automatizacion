package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = {
			//"src/test/resources/features/Login.feature" 
		"features"
		},
        strict = false, plugin = {"pretty",
        "json:target/cucumber_json_reports/Login.json",
        "html:target/login-html"},
        glue = {"pages",
                "steps",
                "utils"})

public class LoginRunner {}

	

