$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:features/Login.feature");
formatter.feature({
  "name": "Login cotizador",
  "description": "  As a User\n  I want to log into cotizador\n  So I can do login",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User can login successfully",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "I navigate to login",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.abrircotizador()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I log into the site with credentials X.JOSUNA:S0p0rt32019",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "I will be in home",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "I will be able to create cotizacion",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});